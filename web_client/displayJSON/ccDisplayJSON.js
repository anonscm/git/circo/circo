/*
Load and display trainings from the circo API. Display list or detailed training.

to display trainings in a web page, insert following lines into html document :
(replace 'link_to_' by the proper value)
    <link href="link_to_circo.css" type="text/css" rel="stylesheet" />
    <script src="link_to_jquery.min.js"></script>
    <script type="application/javascript" src="link_to_ccDisplayJSON.js"></script>
    <script type="text/javascript">
        training(url, idFromUrl)* // display a detailed single training
        trainingList(url,options,detailed_base_url)** // display a training list
    </script>

exemple *: 
training("http://localhost/circo-info/api/trainings/",true); --> calling url must contain "?tra_id=60"
training("http://localhost/circo-info/api/trainings/60");
training("http://localhost/circo-info/api/trainings/60",false);

exemple **
trainingList("http://localhost/circo-info/api/trainings/?page_size=10","category,snippet,keyword")
trainingList("http://localhost/circo-info/api/trainings/type/seminaire/?page_size=10","snippet,keyword","http://localhost/webClient/displayTraining.html")

*/

//TODO : handle cases when no data found, or url not OK

// Call to display a single training
function training(base_url,getIdFromURL)
{
    //getIdFromURL = getIdFromURL || false;
    if (getIdFromURL==true){    
        var tra_id=getUrlParam('tra_id');
        base_url=base_url+tra_id    
    }
    var url=addParam(base_url,'format','jsonp')+"&callback=?";
    var divid="divTraining";
    document.write('<div id="'+divid+'"></div>');
    this.feedcontainer=document.getElementById(divid);
    this.feedcontainer.className = "cc-itemBox"; 
    $(document).ready(function() {
       $.getJSON(url, function(data) {
         displayTraining(data);
      });
    });
}

// Call to a display a training list
// base_detail_url : used to build link to detailed training
function trainingList(base_url,options,base_detail_url)
{
   var url=addParam(base_url,'format','jsonp')+"&callback=?";
   var divid="divTrainingList";
   document.write('<div id="'+divid+'"></div>');
   this.feedcontainer=document.getElementById(divid);
   this.feedcontainer.className = "cc-listBox"; 
   options=options || ""
   $(document).ready(function() {
       $.getJSON(url, function(data) {
         displayTrainingList(data,options,base_detail_url);
      });
    });
}

// Display a single training
function displayTraining(data)
{
    document.title='Training ' +data.tra_id 
    var divResult = document.createElement("div");
    divResult.className = "tra-result"; 

    // Add title
    dataToContener(divResult, "div","tra-title", "",data.tra_title)
    
    // Add context header
    var divContext=document.createElement("div");
    divContext.className="tra-context"
        // Add training type
        dataToContener(divContext, "span","tra-type", "",data.tra_type);
        // Add date
        var strDate=formatDate(data.tra_dtstart,data.tra_dtend);
        dataToContener(divContext, "span","tra-date", '&nbsp',strDate);
        // Add location
        dataToContener(divContext, "span","tra-place", ' &agrave; ',data.tra_urbanArea);
    divResult.appendChild(divContext);
    
    
    // Add title, speakers, objective, keywords and frames
    dataToContener(divResult, "div","tra-speakers", "Orateur(s) : ",data.tra_speakers.join(", "));
    dataToContener(divResult, "div","tra-objective", "",data.tra_objective);
    dataToContener(divResult, "div","tra-keywords", "Mots clefs : ",data.tra_keywords.join(", "));
    dataToContener(divResult, "div","tra-frames", "Cette formation est organis&eacutee dans le cadre de : ",data.tra_frame);
    dataToContener(divResult, "div","tra-frames", "Contributeurs : ",data.tra_contributors.join(", "));
    
    // Add Info table
    var tag=dataToContener(divResult, "div","tra-subtitle", "","Informations pratiques&nbsp&nbsp&nbsp");
    linkToContener(tag,"span","tra-link","Acc&eacuteder au site",data.tra_web);
    var divInfoTable = document.createElement("table");
    divInfoTable.className='tra-info-table';
        dataToTable(divInfoTable,'Date et heure de d&eacutebut', formatDateTime(data.tra_dtstart));
        dataToTable(divInfoTable,'Date et heure de fin', formatDateTime(data.tra_dtend));
        dataToTable(divInfoTable,'Adresse', data.tra_address);
        dataToTable(divInfoTable,"Responsable administratif(ve)", data.tra_admin);
        dataToTable(divInfoTable,"Autres contacts", data.tra_contacts.join(", "));
        dataToTable(divInfoTable,"Pr&eacuterequis", data.tra_prerequisite);
    divResult.appendChild(divInfoTable);
    
    
    // Add training content
    if (data.tra_content.length>0){
        dataToContener(divResult, "div","tra-subtitle", "","Contenu d&eacutetaill&eacute")
        dataToContener(divResult, "div","tra-content", "",data.tra_content)
    }
    
    // Program
    /*
    if (data.tra_program.length>0){
        dataToContener(divResult, "div","tra-subtitle", "","Programme")
        dataToContener(divResult, "div","tra-program", "",data.tra_program)
    }
    */
    this.feedcontainer.appendChild(divResult);
}


// Display a training list
function displayTrainingList(data,options,base_detail_url)
{   
    // List results box
    var divResults=document.createElement("div");
    divResults.className = "tralist-results";

    // Let's add each elements of the list
    $.each(data.results, function(i, item) {

        // Element i result box
        var divResult = document.createElement("div");
        divResult.className = "tralist-result"; 

        // Add title and link to the training detail page
        // The link to the training detailed page is build with base_detail_url
        // unless the redirection option is true and the link to the web page is not empty
        // If no base detail url is defined, the same url with 'displayTraining.html' is used
        if (! base_detail_url){
            base_detail_url="displayTraining.html"
         }
        var strLink=addParam(base_detail_url,"tra_id",data.results[i].tra_id);
        if (data.results[i].tra_web){
            strLink=data.results[i].tra_web;
        }
        linkToContener(divResult,"div","tralist-title", data.results[i].tra_title, strLink);
        
        // date, lieu, cadre
        var strDate=formatDate(data.results[i].tra_dtstart,data.results[i].tra_dtend);
        var divDate=dataToContener(divResult, "div","tralist-date", "",strDate);
        dataToContener(divDate, "span","tralist-place", ", &agrave; : ",data.results[i].tra_urbanArea);
        
        // Organisateurs, cadres, ...
        //dataToContener(divDate, "span","tralist-frames", ", Organis&eacute; par : ",data.results[i].tra_frames.join(", "));

        // Speaker
        dataToContener(divResult, "div","tralist-speakers", "Orateur(s) : ",data.results[i].tra_speakers.join(", "));
        
        // Keywords - optional
        if (options.indexOf('keyword') >-1) {
            dataToContener(divResult, "div","tralist-keywords", "Mots clefs : ",data.results[i].tra_keywords.join(", "));
        }
        
        // Content (=objective) and snippet. Optional
        if (options.indexOf('snippet') >-1) {
            if (data.results[i].tra_snippet != data.results[i].tra_objective){
                //toogle snippet and content
                var contentId='content'+i;
                var snippetId='snippet'+i;
                var strHTMLContent='<a class="showhide" href="javascript:toggle(\''+snippetId+'\',\''+contentId+'\');">&#171;&#171;&#171; </a><br>';
                strHTMLContent+=data.results[i].tra_objective;
                strHTMLContent+='&nbsp;&nbsp;<a class="showhide" href="javascript:toggle(\''+snippetId+'\',\''+contentId+'\');">&#171;&#171;&#171;</a>';
                var strHTMLSnippet='<a title="En savoir plus" class="showhide" href="javascript:toggle(\''+snippetId+'\',\''+contentId+'\');">&#187;&#187;&#187; </a>';
                strHTMLSnippet += data.results[i].tra_snippet;
                strHTMLSnippet+='&nbsp;&nbsp;<a class="showhide" href="javascript:toggle(\''+snippetId+'\',\''+contentId+'\');">&#187;&#187;&#187;</a>';
       
                var divSnippet=dataToContener(divResult, "div","tralist-snippet", "",strHTMLSnippet);
                divSnippet.id=snippetId;
                divSnippet.style.display="block"

                var divContent=dataToContener(divResult, "div","tralist-objective", "",strHTMLContent);
                divContent.id=contentId;
                divContent.style.display="none";

            }else{
                divSnippet = dataToContener(divResult, "div","tralist-snippet", "",data.results[i].tra_snippet);
            }
        }

        // Type (category). Optional
        if (options.indexOf('category') >-1) {
            dataToContener(divResult, "div","tralist-type", "Cat&eacute;gorie : ",data.results[i].tra_type);
        }
        

        // Add this element to the results box
        divResults.appendChild(divResult);
         //experts = experts+data.results[i].tra_title+"<br>";
    });
   
    // Add the results box to the Main box
    this.feedcontainer.appendChild(divResults);
}

// add text to table
function dataToTable(table, label, data){
    //if (data.length>0) {
    if (data) {
        var tr = document.createElement('tr');
        var td1 = document.createElement('td');
        var td2 = document.createElement('td');
        td1.innerHTML=label;
        tr.appendChild(td1);
        td2.innerHTML=data;
        tr.appendChild(td2);
        table.appendChild(tr);
    }
}

// add data to contener. 
// ex : dataToContener(divResult, "div","cc-content", 'contenu : ',data.tra_content)
// contener, tag, className : mandatory
function dataToContener(contener, tag, className, label, data){
    if (data.length>0){
        tagData=document.createElement(tag);
        tagData.className=className
        if (label.length>0){
            tagData.innerHTML=label 
        }
        tagData.innerHTML+=data 
        contener.appendChild(tagData)
        return tagData
    }
}

// add linked data to contener. Mandatory :contener, tag, className, label
function linkToContener(contener, tag, className, label, urlData){
    if (urlData.length>0){
        tagData=document.createElement(tag);
        tagData.className=className;
        tagData.innerHTML+='<a href="' + urlData + '">'+label+'</a>';
        contener.appendChild(tagData);
        return tagData;
    }
}

// util -- add a parameter to an url
function addParam(url, param, value) {
     var a = document.createElement('a');
     a.href = url;
     a.search += a.search.substring(0,1) == "?" ? "&" : "?";
     a.search += encodeURIComponent(param);
     if (value)
         a.search += "=" + encodeURIComponent(value);
     return a.href;
}

// util -- get an url parameter
function getUrlParam(param) {
    var vars = {};
    var parts = document.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,    
    function(m,key,value) {
      vars[key] = value;
    });
    return vars[param];
  }

// data date --> to string
function formatDate(startDate,endDate){
    var lstMonth=['Jan', 'F&eacute;v', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Ao&ucirc;t', 'Sept', 'Oct', 'Nov', 'D&eacute;c'];
    var dStart = new Date(startDate);
    var strStartDate= dStart.getDate()+' ' +lstMonth[dStart.getMonth()]+ ' ' +dStart.getFullYear();
    var dEnd = new Date(endDate);
    var strEndDate= dEnd.getDate()+' ' +lstMonth[dEnd.getMonth()]+ ' ' +dEnd.getFullYear();
    if ( strStartDate ==  strEndDate){
        strDate = 'Le ' + strStartDate;
    }else{
        strDate = 'Du ' + strStartDate;
        strDate += ' au '+ strEndDate;
    }
    return strDate;   
  }

function formatDateTime(strDate){
    var lstMonth=['Jan', 'F&eacute;v', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Ao&ucirc;t', 'Sept', 'Oct', 'Nov', 'D&eacute;c'];
    var dDate = new Date(strDate);
    var strReturn='Le ' + dDate.getDate()+' ' +lstMonth[dDate.getMonth()]+ ' ' +dDate.getFullYear() +', &agrave; '+ dDate.getUTCHours()+'h'+dDate.getUTCMinutes();
    return strReturn;
}

// show/hide 2 elements (snippet and content)
function toggle(snippetId, contentId) {
    var snippet = document.getElementById(snippetId);
    var content = document.getElementById(contentId);
    if(snippet.style.display == "block") {
        snippet.style.display = "none";
        content.style.display = "block";
    }
    else {
        snippet.style.display = "block";
        content.style.display = "none";
    }
} 


