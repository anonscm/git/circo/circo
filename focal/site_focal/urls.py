from django.conf.urls import include, url
from django.views.generic import TemplateView
from django.contrib import admin

#from site_focal.views import IndexView
#from site_calcul.views import DetailView
#from site_calcul.views import CarteView

urlpatterns = [
    url('',include('circo_sites.consult_training.urls')),
    url(r'^$', TemplateView.as_view(template_name="focal/focal_base.html"), name='home'),# localhost:8000/consult
    # Default main Home page



    #url(r'^carte/$', CarteView.as_view(), name='calcul_carte'),
    #url(r'^training/(?P<pk>\d+)/$', DetailView.as_view(), name='calcul_training_detail'),
    url(r'^presentation', TemplateView.as_view(template_name="focal/focal_presentation.html"),name='focal_presentation'),
    url(r'^legal', TemplateView.as_view(template_name="focal/focal_legal.html"),name='focal_legal'),
    url(r'^contact', TemplateView.as_view(template_name="focal/focal_contact.html"),name='focal_contact'),
    url(r'^partners',
        TemplateView.as_view(template_name="focal/focal_partners.html"),
        name='focal_partners'),

    url(r'^robots\.txt', TemplateView.as_view(template_name='robots.txt', content_type='text/plain'),name='robots'),

    url(r'^admin/', admin.site.urls),

    # admin tools (the dashboard)
    url(r'^admin_tools/', include('admin_tools.urls')),

]

#urlpatterns += [url(r'', include('circo.urls')), ]
#urlpatterns += [url(r'', include('circo_sites.consult_training.urls')),
#url(r'^$', FuturTrainingList.as_view(),name='home'),
#]

urlpatterns += [url(r'^api/',include('circo_sites.api.urls')), ]
#urlpatterns += [url(r'^consult/',include('circo_sites.consult_training.urls')), ]