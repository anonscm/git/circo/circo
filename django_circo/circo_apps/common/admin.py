# -*- coding: utf-8 -*-
# Admin pages for the common database

from django.contrib import admin
from circo_apps.common.models import *
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.utils.translation import  ugettext_lazy as _
from django.utils.html import mark_safe, escape
from django.core.urlresolvers import reverse
from django.contrib.auth.forms import UserChangeForm
from circo.ccForms import ccRequiredInlineFormSet

#---------------------------------------------------------------------------
# User admin

class ccUserChangeForm(UserChangeForm):
    """
    Override django UserChangeForm. Add some contraints: the fields email,
    first_name and last_name are mandatory
    """
    def __init__(self, *args, **kwargs):
        super(ccUserChangeForm, self).__init__(*args, **kwargs)
        self.fields['email'].required = True
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True

'''
class UserProfileInline(admin.StackedInline):
    """
    Add the user profile to the use admin page
    """
    model = UserProfile
    can_delete = False
    verbose_name = 'Entité'
    verbose_name_plural = 'profile - Entité responsable de la saisie'
    formset = ccRequiredInlineFormSet
'''

# Add link to Person
class UserPersonInline(admin.StackedInline):
    """
    Add some 'Person' fields to the user admin page
    --> this is a way to check that the user is linked to the proper person.
    The person entities can be changed from this page
    """
    model = Person
    can_delete = False
    verbose_name = 'Personne'
    verbose_name_plural = 'Informations personne'
    fields=('per_first_name','per_last_name','per_email','per_entities')
    readonly_fields =('per_first_name','per_last_name','per_email')
    filter_horizontal = ('per_entities',)

    def get_readonly_fields(self, request, obj=None):
        """
        If the user does not exists yet (add page), the entities are readonly.
        If the user's corresponding person is found, there is the possibility
        to modify the person entities in the user Admin page.
        """
        person=Person.objects.filter(user=obj)
        if person:
            return self.readonly_fields
        else:
            return self.readonly_fields + ('per_entities',)

    def person_link(self, obj):
        """ Add a link to the person change form
        """
        change_url = reverse('admin:common_person_change', args=(obj.pk,))
        return mark_safe('<b><a href="%s">Lien vers la fiche personne %s</a></b>' %(change_url,obj))
    person_link.short_description = 'Personne'

class ccUserAdmin(UserAdmin):
    """
    User admin page
    """
    #list_display = UserAdmin.list_display + ('custom_group','user_entity',)
    list_display = UserAdmin.list_display + ('custom_group',)
    search_fields = UserAdmin.search_fields + ('userprofile__ent__ent_short_name',)
    #inlines = (UserProfileInline,UserPersonInline)
    inlines = (UserPersonInline,)
    form = ccUserChangeForm
    save_on_top=True

    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )

    def get_inline_instances(self, request, obj=None):
        """
        If user doest not exists (add form) : do not display the inlines
        """
        if obj is None:
            return []
        else:
            return [inline(self.model, self.admin_site) for inline in self.inlines]

    '''
    def user_entity(self, obj):
        """
        Get the user entities (from the 'person' table')
        """
        return obj.userprofile
    user_entity.short_description='Saisies pour l\'entité'
    '''

    def custom_group(self, obj):
        """ get group, separate by comma, and display empty string if user
        has no group
        """
        return ','.join([g.name for g in obj.groups.all()]) if obj.groups.count() else ''
    custom_group.short_description='Accès'

admin.site.unregister(User)
admin.site.register(User, ccUserAdmin)

#---------------------------------------------------------------------------
# Country admin pages
admin.site.register(Country)

#---------------------------------------------------------------------------
# Entity type admin pages
class EntityTypeAdmin(admin.ModelAdmin):
    """
    Entity Type admin page
    """
    list_display=('ety_name','ety_desc')

admin.site.register(EntityType,EntityTypeAdmin)

#---------------------------------------------------------------------------
# Entity admin pages
class EntityAdmin(admin.ModelAdmin):
    """
    Entity admin page
    """
    list_display=('ent_short_name','ent_full_name','ety')
    search_fields = ['ent_short_name','ent_full_name']
    list_filter = ('ety',)
    save_on_top=True
    
admin.site.register(Entity,EntityAdmin)

#---------------------------------------------------------------------------
# Urban Area admin pages
#import autocomplete_light
'''
class UrbanAreaForm(autocomplete_light.ModelForm):
    class Meta:
        model = UrbanArea
        autocomplete_fields = ('ctr',)
        fields = '__all__'

from django_extensions.admin import ForeignKeyAutocompleteAdmin

class PermissionAdmin(ForeignKeyAutocompleteAdmin):
    # User is your FK attribute in your model
    # first_name and email are attributes to search for in the FK model
    related_search_fields = {
       'user': ('first_name', 'email'),
    }

    fields = ('user', 'avatar', 'is_active')
'''

class UrbanAreaAdmin(admin.ModelAdmin):
#class UrbanAreaAdmin(ForeignKeyAutocompleteAdmin):
    """
    Urban Area admin page
    """
    #form = UrbanAreaForm
    list_display = ('urb_name', 'ctr')
    search_fields = ['urb_name']
    list_filter = ('ctr',)
    related_search_fields = {
       'ctr': ('ctr_name',),
    }


admin.site.register(UrbanArea,UrbanAreaAdmin)


#---------------------------------------------------------------------------
# Person admin pages

class PersonAdmin(admin.ModelAdmin):
    """
    Person admin page
    """
    filter_horizontal = ('per_entities',)
    readonly_fields = ('user',)
    list_display = ('per_last_name','per_first_name','get_entities')
    search_fields = ('per_last_name','per_first_name', 'per_entities__ent_short_name')
    save_on_top=True

    def user_link(self, obj):
        """
        Add a link to the user admin page. Should be seen only by user with
        proper rights (currently superadmin)
        """
        change_url = reverse('admin:auth_user_change', args=(obj.user.pk,))
        return mark_safe('<b><a href="%s">Lien vers le compte utilisateur de %s</a></b>' %(change_url,obj.user))
    user_link.short_description = 'Compte utilisateur'


    def get_readonly_fields(self, request, obj=None):
        """
        the readonly fields list depends of the user rights.
        """
        # Add links only for change pages - not add pages
        lstReadonlyFields=self.readonly_fields
        if obj:
            # The case of the change form
            #lstReadonlyFields += ('get_input_entity',)
            if request.user.is_superuser:
                lstReadonlyFields += ('user_link',)
            elif obj.user==request.user:
                lstReadonlyFields += ('get_user_group',)
            else:
                pass
        return lstReadonlyFields

    def get_queryset(self, request):
        """
        User identify by 'personal_data' group can only access their own dataè
        """
        qs = super(PersonAdmin, self).get_queryset(request)
        if 'personal_data' in request.user.groups.values_list('name',flat=True):
            qs=qs.filter(user=request.user)
        return qs

    def has_add_permission(self, request):
        """
        If user group is personal_data, the user cannot add a person.
        """
        perm=True
        if 'personal_data' in request.user.groups.values_list('name',flat=True):
            perm=False
        return perm

admin.site.register(Person,PersonAdmin)

#---------------------------------------------------------------------------
# Keyword admin pages
class KeywordAdmin(admin.ModelAdmin):
    """
    Keyword admin pages
    """
    list_display = ('key_keyword', 'key_desc')
    search_fields = ('key_keyword', 'key_desc')

admin.site.register(Keyword,KeywordAdmin)

#---------------------------------------------------------------------------
# Show the logs (superuser only)
# from https://djangosnippets.org/snippets/2484/
# currently disabled

from django.contrib.admin.models import LogEntry, DELETION

class LogEntryAdmin(admin.ModelAdmin):
    """
    Log admin pages. Superuser only
    """

    date_hierarchy = 'action_time'

    list_filter = [
        'user',
        'content_type',
        'action_flag'
    ]

    search_fields = [
        'object_repr',
        'change_message'
    ]

    list_display = [
        'action_time',
        'user',
        'content_type',
        'object_link',
        'action_flag',
        'change_message',
    ]

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return request.user.is_superuser and request.method != 'POST'

    def has_delete_permission(self, request, obj=None):
        return False

    def object_link(self, obj):
        if obj.action_flag == DELETION:
            link = escape(obj.object_repr)
        else:
            ct = obj.content_type
            link = u'<a href="%s">%s</a>' % (
                reverse('admin:%s_%s_change' % (ct.app_label, ct.model), args=[obj.object_id]),
                escape(obj.object_repr),
            )
        return link
    object_link.allow_tags = True
    object_link.admin_order_field = 'object_repr'
    object_link.short_description = u'object'

    def get_queryset(self, request):
        return super(LogEntryAdmin, self).queryset(request) \
            .prefetch_related('content_type')

    def get_readonly_fields(self, request, obj=None):
        return LogEntry._meta.get_all_field_names()

#admin.site.register(LogEntry, LogEntryAdmin)
