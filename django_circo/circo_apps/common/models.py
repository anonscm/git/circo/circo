# -*- coding: utf-8 -*-
"""
Circo model for the common database
This part of the model can be used by other circo apps (training, ...)
"""

from django.db import models
from django.contrib.auth.models import User
from circo.ccFields import *

#---------------------------------------------------------------------------
class Country(models.Model):
    ctr_id = models.AutoField(primary_key=True)
    ctr_name = ccCharField(u'Pays',unique=True,null=False,blank=False)
    auth_group='data'
    # think about : add/del only permission? Not modified? Del is oK is on
    # delete cascade is false, and there seems to be a pb with this in django
    # 1.7 and postgresql.
    class Meta:
        verbose_name=u'Pays'
        verbose_name_plural=u'Pays'
        ordering=['ctr_name']
    def __unicode__(self):
        return self.ctr_name

#---------------------------------------------------------------------------
class EntityType(models.Model):
    ety_id = models.AutoField(primary_key=True)
    ety_name = ccCharField(u"Type d'entité",unique=True,null=False,blank=False)
    ety_desc = ccLongCharField(u"Description",null=True,blank=True)
    
    auth_group='data_admin'
    class Meta:
        verbose_name=u'Type d\'entité'
        verbose_name_plural=u'Types d\'entité'
        ordering=['ety_name']
    
    def __unicode__(self):
        uReturn=self.ety_name
        if self.ety_desc:
            uReturn+= " ("+self.ety_desc+")"
        return uReturn

#---------------------------------------------------------------------------
class Entity(models.Model):
    ent_id = models.AutoField(primary_key=True)
    ety = ccForeignKey(EntityType, verbose_name=u"Type d'entité",null=False,blank=False)
    ent_short_name = ccCharField(u"Nom court de l'entité",unique=True,null=False,blank=False)
    ent_full_name = ccCharField(u"Nom détaillé de l'entité",max_length=100,size=88,null=True,blank=True)
    ent_presentation = models.TextField(u'Présentation',null=True,blank=True)
    ent_web = ccURLField(u'web',null=True,blank=True)
    #add = ccForeignKey(Address, verbose_name='Adresse')
    ent_address=models.TextField(u'Adresse',null=True,blank=True)
    
    auth_group='data'
    class Meta:
        verbose_name=u'Entité'
        ordering = ['ent_short_name']
    def __unicode__(self):
        return self.ent_short_name

#---------------------------------------------------------------------------
class UrbanArea(models.Model):
    urb_id = models.AutoField(primary_key=True)
    urb_name = ccCharField(u"Agglomération",null=False,blank=False)
    #ctr = models.ForeignKey(Country,verbose_name=u'Pays',default=1,null=False,blank=False) # 1=France
    ctr = ccForeignKey(Country,verbose_name=u'Pays',default=1,null=False,blank=False) # 1=France
    auth_group='data'
    class Meta:
        verbose_name=u'Agglomération'
        ordering = ['urb_name']
        unique_together = ['urb_name','ctr']
    def __unicode__(self):
        return self.urb_name + ', ' + unicode(self.ctr)

    def get_urb_name(self):
        """ returns the urban area name, without the country name"""
        return self.urb_name
    get_urb_name.short_description = "Agglo"

#---------------------------------------------------------------------------
class Keyword(models.Model):
    key_id = models.AutoField(primary_key=True)
    key_keyword = ccCharField(u'Mot Clef',unique=True,null=False,blank=False)
    key_desc=ccCharField(u'Définition',null=True,blank=True)
    
    auth_group='data'
    class Meta:
        verbose_name = u'Mot clef'
        verbose_name_plural = u'Mots clef'
        ordering=['key_keyword']
    
    def __unicode__(self):
        return self.key_keyword

#---------------------------------------------------------------------------
class Person(models.Model):
    per_id = models.AutoField(primary_key=True)
    user = models.OneToOneField(User,null=True,blank=True,verbose_name=u'Login')
    #user = models.ForeignKey(User,verbose_name='Login id',db_index=True,null=True,blank=True)
    per_first_name = ccCharField(u'Prénom',null=False,blank=False)
    per_last_name = ccCharField(u'Nom',null=False,blank=False)
    per_email = ccEmailField(null=True,blank=True)
    #per_web = ccURLField() # on met le web si c'est un expert
    per_entities=models.ManyToManyField(Entity,related_name='ent_persons',verbose_name=u'Entités',blank=True)
    
    auth_group='personal_data'
    class Meta:
        verbose_name=u'Personne'
        ordering = ['per_last_name','per_first_name']
        unique_together = ['per_last_name','per_first_name']
    
    def __unicode__(self):
        return "%s, %s"%(self.per_last_name, self.per_first_name)

    def get_name(self):
        """ get Person's name with first name first
        """
        return "%s %s"%(self.per_first_name, self.per_last_name)
    get_name.short_description = "Nom"

    def get_name_last_first(self):
        """ get Person's name with last name fist
        """
        return "%s %s"%(self.per_last_name, self.per_first_name)
    get_name_last_first.short_description = "Nom"

    def get_entities(self):
        """ Get the person entities as string
        """
        return ', '.join([a.ent_short_name for a in self.per_entities.all()])
    get_entities.short_description = "Entité(s)"

    def get_entities_lst(self):
        return [a.ent_short_name for a in self.per_entities.all()]

    '''
    def get_input_entity(self):
        """ Get the person users profile input entity
        """
        entity=None
        if self.user:
            entity=UserProfile.objects.get(user=self.user).ent
        return entity
    get_input_entity.short_description='Saisies au nom de l\'entité'
    '''

    def get_user_group(self):
        user_groups=None
        if self.user:
            user_groups=','.join([g.name for g in self.user.groups.all()]) if self.user.groups.count() else ''
        return user_groups
    get_user_group.short_description='Droits d\'accès'

'''
#---------------------------------------------------------------------------
class UserProfile(models.Model):
    user = models.OneToOneField(User)
    ent = models.ForeignKey(Entity,verbose_name='Entité',null=False,blank=False)
    auth_group='personal_data' # ou admin data?

    class Meta:
        verbose_name = 'Profile'
        verbose_name_plural = 'profile'

    def __unicode__(self):
        return self.ent.ent_short_name
'''
#----------------------------------------------------------------------------
# Synchronise django user authentification table and person table.
# synchronise first_name, last_name and mail.

from django.db.models.signals import post_save
from django.dispatch import receiver

@receiver(post_save, sender=User)
def sync_user_person(sender, instance, **kwargs):
    """
    watch the create user signal, and update the person table
    Use update() and bulk_insert instead of save() to avoid infinite recursion
    with sync_person_user
    """
    user=instance
    if user.is_staff and user.first_name and user.last_name:
        # Get the person with login_id=user.id :
        persons=Person.objects.filter(user=user)
        # if the person exists, save the first_name, last_name and mail.
        if persons:
            persons.update(per_first_name=user.first_name,per_last_name=user.last_name)
            if user.email:
                persons.update(per_email=user.email)
          # if not, get the person with same first name and last name.
        else:
            persons=Person.objects.filter(per_first_name=user.first_name,per_last_name=user.last_name)
            # if a person with same name and last name exists
            if persons:
                persons.update(user=user)
                if user.email:
                    #person.per_email=user.email
                    persons.update(per_email=user.email)
            # create person
            else:
                Person.objects.bulk_create([Person(user=user,
                              per_first_name=user.first_name,
                              per_last_name=user.last_name,
                              per_email=user.email)])

@receiver(post_save, sender=Person)
def sync_person_user(sender, instance, **kwargs):
    """
    watch the save person signal and update the table user.
    Use update() instead of save() to avoid infinite recursion with
    sync_user_person
    """
    person=instance
    if person.user:
        per_user=User.objects.filter(id = person.user.id)
        per_user.update(first_name=person.per_first_name,last_name=person.per_last_name)
        if person.per_email:
            per_user.update(email=person.per_email)