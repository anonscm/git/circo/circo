# -*- coding: utf-8 -*-
__author__ = 'bligny'

from django.apps import AppConfig

class CommonConfig(AppConfig):
    """
    Common app config
    """
    name = 'circo_apps.common'
    verbose_name = u'Données Générales'