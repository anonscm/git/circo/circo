from django.db import models

# Pour test

class Project(models.Model):
    pro_id = models.AutoField(primary_key=True)
    pro_title = models.CharField(u'Titre',unique=True, max_length=100)

    auth_group='data'
    class Meta:
        #db_table = u'COUNTRY'
        verbose_name='Projet'
    def __unicode__(self):
        return self.pro_title
