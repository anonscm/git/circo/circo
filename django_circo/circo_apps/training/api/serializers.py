# -*- coding: utf-8 -*-
from circo_apps.training.models import *
from rest_framework import serializers

class TrainingSerializer(serializers.ModelSerializer):
    tra_type = serializers.StringRelatedField(source='trt')
    tra_urbanArea = serializers.StringRelatedField(source='urb')
    tra_calendar = serializers.StringRelatedField(source='cal')
    tra_speakers = serializers.StringRelatedField(many=True)
    tra_keywords = serializers.StringRelatedField(many=True)
    tra_contacts = serializers.StringRelatedField(many=True)
    tra_contributors = serializers.StringRelatedField(many=True)

    class Meta:
        model = Training

class TrainingListSerializer(TrainingSerializer):
    tra_snippet = serializers.StringRelatedField(source='get_snippet')
    class Meta(TrainingSerializer.Meta):
        fields = (
            'tra_id', 'tra_type','tra_title', 'tra_dtstart', 'tra_dtend',
            'tra_urbanArea','tra_address','tra_web', 'tra_calendar', 'tra_frame',
            'tra_speakers','tra_keywords','tra_contacts','tra_contributors',
            'tra_snippet', 'tra_objective','tra_dtcreated','tra_dtmodified',
            'tra_contact_email','tra_latitude','tra_longitude',
        )

class TrainingDetailSerializer(TrainingSerializer):
    class Meta(TrainingSerializer.Meta):
        fields = (
            'tra_id', 'tra_type','tra_title', 'tra_dtstart', 'tra_dtend',
            'tra_urbanArea','tra_address','tra_web', 'tra_calendar', 'tra_frame',
            'tra_speakers','tra_keywords','tra_contacts','tra_contributors',
            'tra_objective','tra_content', 'tra_prerequisite', 'tra_dtcreated',
            'tra_dtmodified','tra_contact_email','tra_latitude','tra_longitude',
            'tra_level',
        )
