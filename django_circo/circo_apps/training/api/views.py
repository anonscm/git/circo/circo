# -*- coding: utf-8 -*-

from circo_apps.training.api.serializers import *
from circo_sites.api.ccRest import ccReadOnlyModelViewSet
from django.contrib.syndication.views import Feed
from datetime import date
from rest_framework import filters
from circo_apps.training.models import Training
import django_filters

class TrainingFilter(filters.FilterSet):
    urbanArea = django_filters.CharFilter(name="urb__urb_name",
                                          lookup_expr='icontains')
    entity = django_filters.CharFilter(name="tra_contributors__ent_short_name",
                                          lookup_expr='icontains')
    class Meta:
        model = Training
        fields = ['entity', 'urbanArea']

class TrainingViewSet(ccReadOnlyModelViewSet):
    """
    Training View set (the rest framework will generate the list view and
    retrieve view (= view one list element)
    """
    queryset = Training.objects.all()
    list_serializer_class = TrainingListSerializer
    detail_serializer_class = TrainingDetailSerializer
    filter_class = TrainingFilter

class FuturTrainingViewSet(TrainingViewSet):
    queryset=Training.objects.filter(tra_dtend__gte=date.today()).order_by('tra_dtstart')

#------------------------------------------------------------------------------
# Feeds

# virtual class for the training feeds
class TrainingFeed(Feed):

    title = "Liste des dernières formations"
    link = "rss" # racine du site de distribution des données

    # override eventualy by child classes
    def items(self):
        #pass
        #return Training.objects.order_by('-tra_dtstart')[:10]
        #return Training.objects.filter(tra_status='VALIDATED').order_by('-tra_dtstart')[:10]
        return Training.objects.filter(tra_status='VALIDATED')

    def item_title(self, item):
        return item.tra_title

    def item_description(self, item):
        # Location + speaker + short description
        # speakers : needs to simplify the database - wait.
        #return item.tra_objective
        strDescription=item.urb.urb_name + ' - '
        #if  item.tra_place:
        #    strDescription+='Lieu : '+item.tra_place+' ; '
        #if item.tra_length:
        #    strDescription+=u'Durée : ' + item.tra_length+' ; '
        # Keywords
        keywords=item.tra_keywords.all()
        if keywords:
            for keyword in keywords:
                strDescription += keyword.key_keyword +' - '
            strDescription = strDescription.rstrip(' - ')
            strDescription+='. '

        if item.tra_objective:
            if strDescription:
                strDescription+='<br>'
            strDescription+=item.tra_objective
        return strDescription

    def item_categories(self,item):
        # Return the training type
        return (item.trt,)

    def item_pubdate(self,item):
        return item.tra_dtstart
        #return datetime.combine(item.tra_start_date, time())

    def item_author_name(self,item):
        # return the entities (contributors)
        strReturn=''
        entities=item.tra_contributors.all()
        if entities:
            strReturn+=u', organisé par: '
            for entity in entities:
                strReturn += entity.ent_short_name +', '
        return strReturn.rstrip(', ')         # remove the last ', '

    # item_link is only needed if NewsItem has no get_absolute_url method.
    def item_link(self, item):
        return item.get_consult_url()


class FuturTrainingFeed(TrainingFeed):
    title = "Liste des formations à venir ou en cours"

    def items(self):
        #return Training.objects.order_by('-tra_start_date')[:5]
        trainings=super(FuturTrainingFeed,self).items()
        return trainings.filter(tra_dtend__gte=date.today()).order_by('tra_dtstart')

