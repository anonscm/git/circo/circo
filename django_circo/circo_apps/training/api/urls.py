# -*- coding: utf-8 -*-

from django.conf.urls import url ,include
from circo_apps.training.api.views import TrainingViewSet, FuturTrainingFeed, \
                FuturTrainingViewSet #,TrainingDetail
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'trainings/futur', FuturTrainingViewSet)
router.register(r'trainings', TrainingViewSet)

urlpatterns = [
    # API rest
    url(r'', include(router.urls)),
    # Feed
    url(r'feed/futur$', FuturTrainingFeed()),
]
