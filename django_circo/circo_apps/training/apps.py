# -*- coding: utf-8 -*-
from django.apps import AppConfig

class TrainingConfig(AppConfig):
    """
    training app Config
    """
    name = 'circo_apps.training'
    verbose_name = "Formations"
