    # -*- coding: utf-8 -*-
# Django model for the training application

from django.db import models
from django.contrib.auth.models import User
from circo.ccFields import *
from circo_apps.common.models import *
from datetime import datetime
from circo.utils import get_location
from django.conf import settings
#---------------------------------------------------------------------------
class Calendar(models.Model):
    cal_id = models.AutoField(primary_key=True)
    cal_name = ccCharField(u'Calendrier',unique=True,null=False,blank=False)
    cal_desc = ccLongCharField(u'Description',null=True,blank=True)
    cal_entities=ccManyToManyField(Entity,related_name='ent_calendars',verbose_name=u'Entités',blank=True)
    per_admin = ccForeignKey(Person,related_name='cal_resp_admin',verbose_name=u'Contact Principal',blank=True,null=True)
    cal_web = ccURLField(null=True,blank=True)
    #TODO : add creation/modification date and user owner

    # Internal fields - automatically filled
    # Don't put default value for the 2 following fields : then the middleware
    # that fill them will not properly work
    cal_created_by = ccForeignKey(User,verbose_name=u'Créé par',null=False,editable=False,related_name='cal_created_by')
    cal_modified_by = ccForeignKey(User,verbose_name=u'Modifié par',null=False,editable=False,related_name='cal_modified_by')
    cal_dtcreated = ccDateTimeField(u'Date de création',auto_now_add=True,editable=False)
    cal_dtmodified = ccDateTimeField(u'Date de modification',auto_now=True,editable=False)

    auth_group='personal_data'
    class Meta:
        verbose_name=u'Calendrier'
        ordering=['cal_name']
    def __unicode__(self):
        return self.cal_name

    def get_entities(self):
        """
        Return the calendar entities as string
        """
        return ', '.join([a.ent_short_name for a in self.cal_entities.all()])
    get_entities.short_description = "Entité(s)"

# Test pour l'agenda des maths
#class Calendar2(Calendar):
#    class Meta:
#        proxy = True

#---------------------------------------------------------------------------
class TrainingType(models.Model):
    trt_id = models.AutoField(primary_key=True)
    trt_name = ccCharField(u'Type de formation',unique=True,null=False,blank=False)
    trt_desc = ccLongCharField(u'Description',null=True,blank=True)
    
    auth_group='data_admin'
    class Meta:
        verbose_name=u'Type de formation'
        verbose_name_plural=u'Types de formation'
        ordering=['trt_name']
    def __unicode__(self):
        return self.trt_name

#---------------------------------------------------------------------------
    '''
class TrainingStatus(models.Model):
    """Code values: DRAFT, VALIDATED, CANCELED"""
    trs_id = models.AutoField(primary_key=True)
    trs_code = ccCharField(u'Statut de la formations',unique=True,null=False,blank=False)
    trs_desc = ccLongCharField(u'Description',null=True,blank=True)

    auth_group='data_admin'
    class Meta:
        verbose_name='Status de la formation'
    def __unicode__(self):
        return self.trs_code
'''
#---------------------------------------------------------------------------
class Training(models.Model):

    STATUS_CHOICES=(
        ('VALIDATED','Publiée'),
        ('DRAFT','En cours de saisie'),
        ('CANCELLED','Annulée'),
    )

    tra_id = models.AutoField(primary_key=True)
    trt = ccForeignKey(TrainingType,verbose_name=u'Type',null=False,blank=False)
    urb = ccForeignKey(UrbanArea,related_name='urban_area',verbose_name=u'Agglomération',null=False,blank=False)
    tra_title = ccLongCharField(u'Titre',null=False,blank=False) # same training may occur every year. Unique with date
    tra_dtstart = ccDateTimeField(u'Date de début',default=datetime.now(),null=False,blank=False)
    tra_dtend = ccDateTimeField(u'Date de fin',null=False,blank=False)
    tra_status = models.CharField(u'Statut',null=False,blank=False, choices=STATUS_CHOICES, default='VALIDATED',max_length=150)
    tra_web = ccURLField(null=True,blank=True)
    tra_day = models.BooleanField(u'Toute la journée',default=False,null=False)
    cal = ccForeignKey(Calendar,related_name='cal_trainings',verbose_name=u'La formation est associée à ce calendrier',null=True,blank=True)
    
    #add = ccForeignKey(Address,related_name='address',verbose_name='Addresse')
    tra_address=ccLongCharField(u'Adresse',null=True,blank=True)
    per_admin = ccForeignKey(Person,related_name='tra_resp_admin',verbose_name=u'Contact principal',null=True,blank=True)
    tra_frame = ccLongCharField(u'Cadre',null=True,blank=True)
    #tra_length = ccCharField(u'Information complémentaire sur la durée', help_text='Champ texte (45\', 2h, 3j, 1 semaine...)',null=True,blank=True)
    tra_objective = models.TextField(u'Objectif',null=True,blank=True) # No HTML. Objective or Summury? remettre le champs à tra_objective
    tra_content = models.TextField(u'Contenu',null=True,blank=True) # No HTML
    tra_prerequisite = models.TextField(u'Prérequis',null=True,blank=True) # No HTML
    #tra_program_url : to be removed. load a pdf instead. waiting for deletion : hide it
    tra_program_url = ccURLField(u'Lien vers le programme', null=True,blank=True,editable=False) # Allow HTML? Flatpage? (check security issue)
    tra_contact_email = ccEmailField(u'Email de contact', null=True,blank=True)
    tra_level=ccLongCharField(u'Niveau de la formation',null=True,blank=True)
    tra_public=ccLongCharField(u'Public Visé',null=True,blank=True)
    # TODO : Pouvoir charger des pdf et autre docs
    # With flat pages, possiblity to use a page? (ie something like tra_HTMLContent ?)
        
    # champs many2many
    tra_keywords=ccManyToManyField(Keyword,verbose_name=u'Mots Clefs',blank=True)
    tra_speakers=ccManyToManyField(Person,related_name='speakers+',blank=True,verbose_name=u'Orateurs')
    tra_contacts=ccManyToManyField(Person,related_name='contacts+',blank=True,verbose_name=u'Autres contacts')
    tra_contributors=ccManyToManyField(Entity,verbose_name=u"entites contributrices", blank=True)

     # Internal fields - automatically filled
    tra_latitude = models.FloatField(u'Latitude',null=True,blank=True)
    tra_longitude = models.FloatField(u'Longitude',null=True,blank=True)
    tra_created_by = ccForeignKey(User,verbose_name=u'Créée par',null=False,editable=False,related_name='tra_created_by')
    tra_modified_by = ccForeignKey(User,verbose_name=u'Modifiée par',null=False,editable=False,related_name='tra_modified_by')
    tra_dtcreated = ccDateTimeField(u'Date de création',auto_now_add=True,editable=False)
    tra_dtmodified = ccDateTimeField(u'Date de modification',auto_now=True,editable=False)
    #tra_created_by_ent = ccForeignKey(Entity,verbose_name=u'Entité responsable de la saisie',related_name='tra_created_by_ent',null=True,blank=True)
    # uuid ? UUIDField ; https://docs.djangoproject.com/en/dev/ref/models/fields/#uuidfield - next django version

    auth_group='personal_data'
    class Meta:
        verbose_name=u'Formation'
        ordering=['-tra_dtstart','tra_title']
        unique_together = ['tra_title','tra_dtstart']
    def __unicode__(self):
        return self.tra_title

    def get_dtstart_date(self):
        """
        Return the start date of the training as a string.
        Format : 12 dec 20145
        """
        return self.tra_dtstart.strftime("%d %b %Y")
    get_dtstart_date.short_description='Date de début'

    def get_date(self):
        """
        Return the start date and end date of the training. Type : string
        Format :
            - '12 dec 2015' if start date = end date
            - 'Du 12 dec au 13 dec 2015' if end date > start date
        """
        strReturn=""
        if self.tra_dtstart.strftime("%d %b %Y")==self.tra_dtend.strftime("%d %b %Y"):
            strReturn=self.tra_dtstart.strftime("%d %b %Y")
        else:
            strReturn="Du "+self.tra_dtstart.strftime("%d %b")
            strReturn+=" au "+self.tra_dtend.strftime("%d %b %Y")
        return strReturn
    get_date.short_description='Date(s)'

    def get_contributors(self):
        """
        Return the training's contributors. Type : unicode.
        The contributors are the entities contributing to the training.
        """
        return ', '.join([a.ent_short_name for a in self.tra_contributors.all()])
    get_contributors.short_description = "Entité(s)"

    def get_snippet(self):
        """
        returns the begining of the training's objective. Type= unicode
        """
        uSnippet=self.tra_objective
        if uSnippet:
            if len(uSnippet)>120:
                uSnippet=uSnippet[0:120]
        return uSnippet

    def get_keywords(self):
        """
        :return: the training keywords. Type : unicode
        """
        return ', '.join([a.key_keyword for a in self.tra_keywords.all()])
    get_keywords.short_description = "Mots clefs"

    def get_contacts(self):
        """
        :return: the contact list. Type : unicode
        """
        return ', '.join([per.get_name() for per in self.tra_contacts.all()])
    get_contacts.short_description = "Contacts"

    def get_speakers(self):
        """
        :return: the speakers list. Type : unicode
        """
        return ', '.join([per.get_name() for per in self.tra_speakers.all()])
    get_contacts.short_description = "Orateurs"

    def get_urb_name(self):
        """ returns the urban area name, without the country name"""
        return self.urb.urb_name
    get_urb_name.short_description = "Agglo"

    def get_speakers_lst(self):
        """
        Returns the list of the trainings' speakers with first name first
        """
        return [per.get_name() for per in self.tra_speakers.all()]

    def get_contacts_lst(self):
        """
        Return the contacts list, with first name first
        """
        return [per.get_name() for per in self.tra_contacts.all()]

    def get_flat_address(self):
        """
        :return: Address without linebreaks. Replace linebreaks by ', '
        """
        uReturn = ', '.join(self.tra_address.splitlines())
        uReturn = uReturn.replace(',,',',').replace(', ,',',')
        return uReturn

    def get_consult_url(self):
        """ Returns the link to training detail page on the circo consult site
        """
        if settings.CC_TRAINING_WEB:
            return settings.CC_TRAINING_WEB + "/training/" + str(self.pk)
        else:
            return None

    def get_training_url(self):
        """ Returns the link to the training tra_web if not null. Else, returns
        the link to the page detail on the circo consult site
        """
        if self.tra_web:
            return self.tra_web
        else:
            return self.get_consult_url()

    '''
        def get_web_view(self):
        uLink=u''
        if self.tra_redirect:
            uLink=self.tra_web
        else:
            if settings.CC_WEB_SITE and settings.CC_TRAINING_URL:
                #if CC_WEB_SITE has a trailing '/', let's remove it
                strWebSite=settings.CC_WEB_SITE.rstrip('/')
                #if CC_TRAINING_URL does not begin by a '/', let's add it
                strTrainingPage='/' + settings.CC_TRAINING_URL.lstrip('/')
                strLink=strWebSite+strTrainingPage+str(self.tra_id)
                uLink=unicode(strLink,'utf8')
        return uLink
    '''

    def update_location(self):

            #geolocator=Nominatim() # geolocator from OSM

            # Try to find the location
            #location=geolocator.geocode(self.tra_address)
            location=get_location(self.tra_address)
            if (location is None):
                location=get_location(self.urb)
                #location=geolocator.geocode(self.urb)

            #if location found, update tra_latitude and tra_longitude
            if (location is not None):
                self.tra_latitude = location.latitude
                self.tra_longitude = location.longitude

    def save(self,*args,**kwargs):
        ''' Before saving, update the latitude and longitude from the adress
        or from the urban area. (if None)
        Check that the start date <= end date
        '''
        if (self.tra_latitude is None or self.tra_longitude is None):
            self.update_location()

        if self.tra_dtend:
            if self.tra_dtend<self.tra_dtstart:
                self.tra_dtend=self.tra_dtstart
        else:
            self.self.tra_dtend=self.tra_dtstart

        super(Training,self).save(*args,**kwargs)