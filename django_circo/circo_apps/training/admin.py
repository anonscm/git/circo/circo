# -*- coding: utf-8 -*-

# django import
from django.contrib import admin
from django.utils.html import mark_safe
from django.core import urlresolvers

# circo import
from circo_apps.training.models import *

#------------------------------------------------------------------------------
# Training type admin page
admin.site.register(TrainingType)

#------------------------------------------------------------------------------
# Calendar admin page
class TrainingCalendarInline(admin.TabularInline):
    """
    Calendar Inline : in the calendar admin page, list the calendar's related
    trainings
    """
    #model = Training.cal.through
    model=Training
    fields=('tra_title','get_date')
    readonly_fields=('tra_title','get_date')
    extra=0
    ordering=('-tra_dtstart',)
    #template='admin/edit_inline/training_tabular.html'
    max_num=0

class CalendarAdmin(admin.ModelAdmin):
    """
    Calendar admin page
    """

    class Media:
        css = {
            "all":("admin/css/small_multiple_choice.css",
                   "admin/css/small_tabular_inline.css",)
        }

    #inlines=[ EntityPersonInline, ]
    #filter_horizontal = ('cal_entities',)
    list_display = ('cal_name','per_admin','get_entities')
    fields=(('cal_name','add_training_link'),'per_admin','cal_desc','cal_web','cal_entities')
    readonly_fields = ('add_training_link','cal_created_by','cal_modified_by','cal_dtcreated','cal_dtmodified')
    inlines=(TrainingCalendarInline,)
    save_on_top=True
    search_fields = ('cal_name','per_admin__per_first_name','per_admin__per_last_name',\
                    'cal_entities__ent_short_name')

    '''
    fieldsets=(
        #(None,{'fields':('tra_title',),}),
                   (None,{
                    'fields':(('cal_name','add_training_link'),'per_admin','cal_desc','cal_web','cal_entities')}
                    ),
                 ('Champs internes',{
                 #'description':OPTIONAL_HELP,
                'fields':(('cal_created_by','cal_modified_by'),('cal_dtcreated','cal_dtmodified'))}
                )
            )
    '''
    def add_training_link(self,obj):
        """
        Add to the calendar admin page a link that redirect to the training
        creation page. This page will be initialized with the current calendar
        values (calendar id and admin responsible)
        """
        link=urlresolvers.reverse('admin:training_training_add')
        link += '?cal='+str(obj.pk)
        return mark_safe('<b><a href="%s">Ajouter une formation à ce calendrier</a></b>'%(link))
    add_training_link.short_description=''

    def get_fields(self, request, obj=None):
        """
        """
        if request.user.is_superuser and obj:
            #return self.fieldsets + ( ('Champs internes',{
            #    'fields':('tra_created_by','tra_modified_by','tra_dtcreated','tra_dtmodified','tra_created_by_ent')}
            #    ),)
            return self.fields + (('cal_created_by','cal_modified_by'),('cal_dtcreated','cal_dtmodified'))
        else:
            return self.fields  + (('cal_created_by','cal_modified_by'),('cal_dtcreated','cal_dtmodified'))

    def get_queryset(self, request):
        """
        Restrict the data visibility for user belonging to group 'personal_data'
        or 'entity_data'
        """
        qs = super(CalendarAdmin, self).get_queryset(request)
        lstPermissions=request.user.groups.values_list('name',flat=True)

        if 'entity_data' in lstPermissions:
            qs=self._get_user_qs(request.user,qs) | \
               self._get_user_entity_qs(request.user,qs)
            qs=qs.distinct()
        elif 'personal_data' in lstPermissions:
            qs=self._get_user_qs(request.user,qs)

        return qs

    def _get_user_qs(self, user, qs=None):
        """
        Person with personal_data rights can access only their personal data :
           - Calendars with per_admin = user
           - Calendars with cal_created_by = user
        """
        if not qs:
            qs=Calendar.objects.all()
        qs=qs.filter(cal_created_by=user) | qs.filter(per_admin__user=user)
        qs=qs.distinct()
        return qs

    def _get_user_entity_qs(self, user, qs=None):
        """
        Person with entity_data rights can access only their entity data :
           - Calendars with created_by_entity = user.entity
           - Calendars with one of cal_entities same than the user profile entity
           - Calendars with one of cal_entities included in the person entities
        """
        #profile_entity=user.userprofile.ent
        person_entities=user.person.per_entities.all()
        if not qs:
            qs=Calendar.objects.all()
        #qs=qs.filter(cal_created_by_ent=profile_entity) | \
        #qs=qs.filter(cal_entities=profile_entity) | \
        qs=qs.filter(cal_entities__in=person_entities)
        qs=qs.distinct()
        return qs

admin.site.register(Calendar,CalendarAdmin)

#------------------------------------------------------------------------------
# Training admin page
class TrainingAdmin(admin.ModelAdmin):
    """
    Training admin page
    """

    class Media:
        css = {
            "all":("admin/css/small_multiple_choice.css",)
        }

    MANTATORY_HELP="<a NAME='mandatory_fields'><b>Zone de saisie des champs obligatoires</b></a><br>\
                   <A HREF='#optional_fields'>Raccourci vers la zone de saisie des informations complémentaires</a><br>\
                   <A HREF='#content_fields'>Raccourci vers la zone de saisie du contenu détaillé de la formation</a><br><br>"

    OPTIONAL_HELP= "<A NAME='optional_fields'><b>Zone de saisie des informations complémentaires</a></b><br>\
                    <A HREF='#mandatory_fields'>Raccourci vers la zone de saisie des champs obligatoires</a><br>\
                   <A HREF='#content_fields'>Raccourci vers la zone de saisie du contenu détaillé de la formation</a><br><br>"

    CONTENT_HELP="<A NAME='content_fields'><b>Zone de saisie du contenu détaillé de la formation</a></b><br>" \
                 "<a HREF='#mandatory_fields'>Raccourci vers la zone de saisie des champs obligatoires</a><br>\
                   <A HREF='#optional_fields'>Raccourci vers la zone de saisie des informations complémentaires</a><br><br>"

    fieldsets=(
        #(None,{'fields':('tra_title',),}),
            ('Informations Générales - champs OBLIGATOIRES', {
                'description':MANTATORY_HELP,
                #  'classes': ('collapse','open'), # pour le moment, marche pas
                'fields':('tra_title','trt',('tra_dtstart','tra_dtend'),
                    'urb','tra_contributors','tra_status')
                }
            ),
            ('Informations Générales Complémentaires', {
                'description':OPTIONAL_HELP,
                'fields':('tra_web','cal','tra_address','tra_frame',
                          'per_admin','tra_contact_email','tra_contacts',
                          'tra_speakers',)
                }
            ),
            ('Contenu de la Formation', {
                'description':CONTENT_HELP,
                'fields':('tra_keywords','tra_objective','tra_content',
                          'tra_prerequisite','tra_level')
                }
            ),
            ('Champs internes', {
                #'fields':(('tra_created_by','tra_modified_by'),('tra_dtcreated','tra_dtmodified'),'tra_created_by_ent')}
                'fields':(('tra_created_by','tra_modified_by'),
                          ('tra_dtcreated','tra_dtmodified'),
                          ('tra_latitude','tra_longitude'))
                }
            ),
        )

    filter_horizontal = ('tra_speakers','tra_contacts','tra_contributors','tra_keywords',)
    readonly_fields = ('tra_created_by','tra_modified_by','tra_dtcreated',
                       'tra_dtmodified','tra_latitude','tra_longitude')
    save_on_top=True
    save_as=True
    list_display=('tra_title','trt','get_dtstart_date','get_urb_name','get_contributors') #'get_date'
    list_filter=('trt','urb')
    date_hierarchy='tra_dtstart'
    search_fields = ('tra_title','trt__trt_name','urb__urb_name',
                    # 'per_admin__per_first_name','per_admin__per_last_name',\
                    'tra_contributors__ent_short_name')

    show_save_and_add_another=False

    def view_on_site(self,obj):
        """
        Returns the public url view on the training
        using this solution, the public url for the trainings can be
        configured in the local_settings file.
        Inspired from the django admin file option.py
        Tried first to define a get_absolute_url in models.py, as read in the
        doc, but since django use a reverse() on this url, it was ko.
        """
        if obj:
            return obj.get_consult_url()
        else:
            return None

    def get_form(self, request, obj=None, **kwargs):
        """
        If the Training add form is called with the calendar id given,
        (ex : http://127.0.0.1:8000/admin/training/training/add/?per_admin=1&cal=1)
        then the admin responsible, the web page, the objective and the
        training's contributors values are initialized, using the calendar
        values.
        """
        get=request.GET
        form = super(TrainingAdmin, self).get_form(request, obj, **kwargs)
        if get.has_key('cal'):
            # init the training values with cal values.
            cal=Calendar.objects.get(pk=get['cal'])
            form.base_fields['per_admin'].initial = cal.per_admin
            form.base_fields['tra_web'].initial = cal.cal_web
            form.base_fields['tra_objective'].initial = cal.cal_desc
            form.base_fields['tra_contributors'].initial=cal.cal_entities.all()
        return form

    def get_fieldsets(self, request, obj=None):
        """
        Add the internal readonly fields to the field set, only for superuser
        Currently disabled : for dev and test purpose, these fields are always
        shown
        """
        if request.user.is_superuser and obj:
            #return self.fieldsets + ( ('Champs internes',{
            #    'fields':('tra_created_by','tra_modified_by','tra_dtcreated','tra_dtmodified','tra_created_by_ent')}
            #    ),)
            return self.fieldsets
        else:
            return self.fieldsets

    '''
    def get_readonly_fields(self,request,obj=None):
        """
        For all user but the super admin, the internal field tra_created_by_ent is readonly
        """
        readonly_fields=self.readonly_fields
        if not(request.user.is_superuser):
            readonly_fields += ('tra_created_by_ent',)
        return readonly_fields
    '''

    def get_queryset(self, request):
        """
        Restrict the data acces for the users with personal_data or entity_data
        rights
        """
        qs = super(TrainingAdmin, self).get_queryset(request)
        #profile_entity=request.user.userprofile.ent
        #person_entities=request.user.person.per_entities.all()

        if 'personal_data' in request.user.groups.values_list('name',flat=True):
            qs=self._get_user_qs(request.user,qs)
        #    qs=qs.filter(Q(tra_created_by=request.user) |
        #                 Q(per_admin__user=request.user))

        if 'entity_data' in request.user.groups.values_list('name',flat=True):
            qs=self._get_user_qs(request.user,qs) | \
               self._get_user_entity_qs(request.user,qs)
            #qs=qs.filter(tra_created_by_ent=profile_entity) | \
            #    qs.filter(tra_contributors=profile_entity) |  \
            #    qs.filter(tra_contributors__in=person_entities)

        # =Q.distinct()
            qs=qs.distinct()
        return qs

    def _get_user_qs(self, user, qs=None):
        """
        Person with personal_data rights can access only their personal data :
           - Trainings with per_admin = user
           - Training with tra_created_by = user
        """
        if not qs:
            qs=Training.objects.all()
        qs=qs.filter(tra_created_by=user) | qs.filter(per_admin__user=user)
        qs=qs.distinct()
        return qs

    def _get_user_entity_qs(self, user, qs=None):
        """
        Person with entity_data rights can access only their entity data :
           - Trainings with created_by_entity = user.entity
           #- Trainings with one of tra_contributors same than the user profile entity
           - Trainings with one of tra_contributors included in the person entities
        """
        #profile_entity=user.userprofile.ent
        person_entities=user.person.per_entities.all()
        if not qs:
            qs=Training.objects.all()
        #qs=qs.filter(tra_created_by_ent=profile_entity) | \
        #    qs.filter(tra_contributors=profile_entity) | \
        qs=qs.filter(tra_contributors__in=person_entities)
        qs=qs.distinct()
        return qs


    def save_model(self, request, model, form, change):
            """action before saving a training"""
            if ('tra_address' in form.changed_data or 'urb' in form.changed_data):
                model.update_location()

            super(TrainingAdmin, self).save_model(request, model, form, change)

admin.site.register(Training,TrainingAdmin)