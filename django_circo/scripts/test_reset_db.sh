# run the script using : sh reset_db.sh
# The virtual env must be activated to run the script

# deletes All tables in the project and re-create them (they will be empty, except for the initial data)

# to be improved : stop if an error occurs (ex : database cannot be deleted because there is another connection)

# Delete database. Needs django_extensions
python ../manage.py reset_db

# Empty the migrations directories
rm -rf ../circo_apps/common/migrations/*
rm -rf ../circo_apps/training/migrations/*
rm -rf ../circo_apps/project/migrations/*
rm -rf ../circo_apps/expert/migrations/*

# Create the tables with some initial data :
# Prepare migration
python ../manage.py makemigrations common
python ../manage.py makemigrations training
python ../manage.py makemigrations project
python ../manage.py makemigrations expert

# Run the SQL
python ../manage.py migrate

# Create superuser
# python ../manage.py createsuperuser

# Update permissions
python ../manage.py update_permissions

# Fixture : insert initial data
python ../manage.py loaddata admin_user
python ../manage.py loaddata start_data
python ../manage.py loaddata test_data

