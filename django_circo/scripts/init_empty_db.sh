# run the script using: sh start_empty_db.sh
# The virtual env must be activated to run the script

# Create the project tables (circo tables, django ones, and tables from other apps)

# Prepare migration
python ../manage.py makemigrations common
python ../manage.py makemigrations training
python ../manage.py makemigrations project
python ../manage.py makemigrations expert

# Run the SQL
python ../manage.py migrate

# Create superuser
python ../manage.py createsuperuser

# Update permissions
python ../manage.py update_permissions

# Fixture : insert initial data
python ../manage.py loaddata start_data

