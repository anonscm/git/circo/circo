# -*- coding: utf-8 -*-
"""
Circo local settings, based on django settings + some circo specific settings.

copy this file in you django site directory or in
django_circo/local/local_settings.py file, and overwrite it with appropriate
values

For more information about the settings, see
https://docs.djangoproject.com/en/1.10/topics/settings/
https://docs.djangoproject.com/en/1.10/ref/settings/
"""

from circo.settings import *

# replace with MySite.urls if not using the circo/local directory
ROOT_URLCONF = 'circo.urls'

# replace 'local' with the proper directory if not using the circo/local
WSGI_APPLICATION = 'local.wsgi.application'

ADMINS = (
     ('admin1 name', 'admin1@mail.fr'),
     ('admin2 nmae','admin2@mail.fr')
)

# Database
# https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'myDBName',
        'USER': 'myUser',
        'PASSWORD': '**********',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}

# Application definition
INSTALLED_APPS = (
                  #'mySite', #optionnal

                  # Circo Applications :
                  'circo', # Mandarory
                  'circo_apps.common', # Mandatory
                  'circo_apps.training',
                  #'circo.project',
                  #'circo.expert',
                  'circo_sites.api',
                  'circo_sites.consult_training',
                  
                  # Tools :
                  'admin_tools',
                  'admin_tools.dashboard',
                  'rest_framework',
                   #'django_extensions',  # additional commands
                  
                  # Django Applications :
                  'django.contrib.admin',
                  'django.contrib.auth',
                  'django.contrib.contenttypes',
                  'django.contrib.sessions',
                  'django.contrib.messages',
                  'django.contrib.staticfiles',

                  #'debug_toolbar.apps.DebugToolbarConfig', # optional - debug tool

                  )

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '************************************************************'

STATIC_ROOT='/var/www/mySite/static'

# SECURITY WARNING: don't run with debug=true on in production!
DEBUG = True
# Must be defined if debug=False (production environment)
#ALLOWED_HOSTS= ['*',]
#TEMPLATE_DEBUG = True

# Internationalization
# https://docs.djangoproject.com/en/1.10/topics/i18n/

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'fr'

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
TIME_ZONE = 'Europe/Paris'

#=============================================================================
# Circo specific settings

# Will appear in the page header of the admin pages
CC_SITE_OWNER= 'CIRCO' # AMIES, Maimosine, ...

# define if this is the dev, test or prod environment
# Use the keywords 'dev', 'test', or 'prod'
CC_ENVIRONMENT='test'

# used to build the link "Voir sur le site" in the training edit page
CC_TRAINING_WEB="/training"

#=============================================================================
# Tools

#optional
REST_FRAMEWORK = {
    'PAGE_SIZE': 100,
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer', # format=json
        'rest_framework.renderers.BrowsableAPIRenderer', # format=api
        'rest_framework.renderers.AdminRenderer', # format=admin
        'circo_sites.api.renderers.XMLRenderer', # format=xml
        'circo_sites.api.renderers.JSONPRenderer', # format=jsonp
        'circo_sites.api.renderers.ICALRenderer', # format=ical
    ),
    'DEFAULT_FILTER_BACKENDS': (
        'rest_framework.filters.DjangoFilterBackend',
    ),
}
