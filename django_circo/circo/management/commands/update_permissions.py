#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from django.contrib.auth.models import Group, Permission
from django.apps import apps
from django.conf import settings
from django.contrib.contenttypes.models import ContentType

# TODO  : log instead of print

class Command(BaseCommand):
    """
    The update_permissions command update the group permissions :
        - It add the groups if they do not exist yet. The groups are currently
           data_admin, data, entity_data, personal_data
        - It add delete / change / add table permission to the groups, according
        to the auth_group attribute of the table class.
    To run update_permissions from command line, use :
        python manage.py update_permissions
    """
    help = 'Update the group permissions. Should be run after db creation, ' \
           'or on table creation/deletion'

    def handle(self, *args, **options):
        """
        Update the groups permissions, according to table class attribute auth_group
        """
        # Create or get the groups
        (data_admin, created) = Group.objects.get_or_create(name='data_admin')
        (data,created) = Group.objects.get_or_create(name='data')
        (personal_data,created) = Group.objects.get_or_create(name='personal_data')
        (entity_data,created) = Group.objects.get_or_create(name='entity_data')

        # for each model in each circo application, add the group permissions if
        # auth_group is defined.

        print 'Update group permissions'
        # get all django application
        # app : app name with package (ex : circo_app.training)
        for app in settings.INSTALLED_APPS:
            # filter only circo application : __name__ begin with 'circo_apps.'
            if ('circo_apps' in app):

                # get appConfig
                strAppLabel = app.split('.')[-1]
                appConfig = None
                models=None
                try :
                    appConfig = apps.get_app_config(strAppLabel)
                    models = appConfig.get_models()
                except:
                    pass

                # get all models (=tables)
                for model in models:

                    if hasattr(model, 'auth_group'):

                        group_name= model.auth_group

                        # Get the three permission per model : add, remove, change.
                        content_type = ContentType.objects.get_for_model(model)
                        permissions = Permission.objects.filter(content_type=content_type)

                        # add the permissions to the proper group, and possibly remove
                        # them from other groups
                        # entity_data : same table rights than data
                        if group_name=='data_admin':
                            for permission in permissions:
                                data_admin.permissions.add(permission)
                                data.permissions.remove(permission)
                                entity_data.permissions.remove(permission)
                                personal_data.permissions.remove(permission)
                        if group_name=='data':
                            for permission in permissions:
                                data.permissions.add(permission)
                                entity_data.permissions.add(permission)
                                data_admin.permissions.add(permission)
                                personal_data.permissions.remove(permission)
                        if group_name=='personal_data':
                            for permission in permissions:
                                personal_data.permissions.add(permission)
                                data_admin.permissions.add(permission)
                                data.permissions.add(permission)
                                entity_data.permissions.add(permission)

                        # add the dashbord permissions? Seems to work without

        print 'Permissions groups updated'

        # Test : what if a table is removed?
