#!/usr/bin/env python
# -*- coding: utf-8 -*-
from circo_apps.training.models import Training
from circo.utils import get_location
from django.core.management.base import BaseCommand

# TODO  : log instead of print

class Command(BaseCommand):
    """
    The update_location command update the trainings geolocation. It search
    the lat and long of the training, using the address. If not found, it
    search it using the urban area name + country name.
    The lat and long are save as training attributes
    """
    help = 'Update the group rights. Should be run after db creation, or table creation/deletion'


    def add_arguments(self, parser):

        parser.add_argument('--not-found',
                            action='store_true',
                            dest='not-found',
                            default=False,
                            help='List the addresses with no location found')

        parser.add_argument('--dry-run',
                            action='store_true',
                            dest='dry-run',
                            default=False,
                            help='List the addresses, urban areas, and their location')


    def handle(self, *args, **options):
        """
        Update the latitude and longitude of the trainings, according to the
        address or the urban area
        """

        print 'Geocoder used : Nominatim, from Open Street Map and google maps'
        trainings=Training.objects.all()

        if options['not-found']:
            print 'List of addresses that could not be geolocated'

            for training in trainings:
                if training.tra_address:
                    location=get_location(training.tra_address)
                    if (location is None):
                        print 'Geolocation failed for training ' + str(training.tra_id)
                        print 'with address = ' + training.tra_address
                else:
                    location=get_location(str(training.urb))
                    if (location is None):
                        print 'Geolocation failed for training ' + str(training.tra_id)
                        print 'with address null and urban area = ' + str(training.urb)

        elif options['dry-run']:
            print 'Location will be updated as following : '

            for training in trainings:
                print '------- training '+str(training.tra_id)+' -------------'
                print 'Address = ' + training.tra_address
                location=None
                if training.tra_address:
                    location=get_location(training.tra_address)

                if (location is None):
                    print 'The training could not be located using the address.'
                    print 'Try to use the urban area : ' +str(training.urb)
                    location=get_location(str(training.urb))

                if (location is not None):
                    print 'latitude : ' +str(location.latitude)
                    print 'longitude : '+str(location.longitude)
                else:
                    print 'GEOLOCATION FAILED'

        else:

            print 'Start geolocation update'
            n=0

            for training in trainings:
                location=None

                # Get the location
                if training.tra_address:
                    location=get_location(training.tra_address)
                if (location is None):
                    location=get_location(str(training.urb))

                if (location is not None):
                    training.tra_longitude = location.longitude
                    training.tra_latitude = location.latitude
                    training.save()
                    n=n+1
                else:
                    print 'Geolocation failed for training ' +str(training.tra_id)

            print 'End geolocation update. '+str(n)+ ' trainings updated.'
