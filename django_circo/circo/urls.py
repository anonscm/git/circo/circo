from django.conf.urls import include, url
from django.conf import settings
from django.contrib import admin
from django.views.generic import TemplateView

"""
circo main routes
When using circo, 3 choices withe the urls
1) use the circo urls without any changes
2) use the circo urls, and append some urls:

    urlpatterns = [
    mySiteURLs
    ]
    urlpatterns += [url(r'', include('circo.urls')), ]
3) Copy paste part of the content of this file in your mySite/urls.py and
modify it as needed

"""

urlpatterns = [
                       
    # Admin Pages. Only pages for enabled app are visible
    # Admin site is not optional
    url(r'^admin/', admin.site.urls),
                         
    # admin tools (the dashboard)
    url(r'^admin_tools/', include('admin_tools.urls')),

    # Default main Home page
    url(r'^$', TemplateView.as_view(template_name="circo/home.html"),name='home'),
]

#include all the circo_sites urls
# these apps must have a urls.py file

apps = settings.INSTALLED_APPS
sites = [app for app in apps if app.startswith('circo_sites')]
for site in sites:
    site_name=site.split('.')[1]
    site_urls=site+'.urls'
    urlpatterns += [url(r'^'+site_name+'/',include(site_urls)), ]




