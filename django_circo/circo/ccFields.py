# -*- coding: utf-8 -*-
"""
This package contains the circo specific fields. It add the following to the
django fields :
    - Resize the textfields or textarea of the field in the admin
    - Modify some initial parameters (index, null, unique, on_delete)
    - Define some default values (label, ...)

some problems encountered:
    - New default attributes may not be taken in account using the db migration
    functionality (ex : null=True)
    reful with migration : check that all OK.
    - on_delete.PROTECT seems not to be working with django 1.7 and postgresql.
    - Using the Textarea, the max_length is not controled during input, but
    cut when saving the data

Limitations:
    - For the char/text type fields, null=True equivalent to blank = true
    - It is not impossible to have a unique field with null values.
    It would be useful for the email fields;

List of the circo fields :
    - ccLongCharField : models.CharField with default max_length=200 widget = textarea 84x2
    - ccCharField : models.CharField with default max_lenth=55, widget=TextInput with size=size or max_length
    - ccURLField : models.URLField with default name='web' and default input size = 86
    - ccEmailField : models.EmailField with default name = 'mèl' and default input size = 50
    - ccDateTimeField : split date and time, and format time :'%H:%M','%Hh%M'
    - ccTimeField : format input : '%Hh%M','%H:%M','%Hh' and output.
"""

#TODO :
#   - ask why on_delete.PROTECT does not work,
#   - remove the use of ccForeignKey until the precedent point is solved
#   - Check heritage pb with custom fields
#   - use constants to define the default values ?

from django.forms import * # replace the * by appropriate name
from django.db import models
from django.core import validators
from django.contrib.admin.widgets import AdminSplitDateTime


class ccLongCharField(models.CharField):
    """
    models.CharField with default max_length=200 and widget = textarea 84x2
    Destination field: long title, address, short description, ...
    200 characters max, input area is a small textarea
    pb : using the Textarea, the max_length is not controled during input, but
    cut when saving the data
    """

    def __init__(self,*args,**kwargs):
        kwargs['max_length']=kwargs.get('max_length',200)
        super(ccLongCharField,self).__init__(*args,**kwargs)

    # See doc https://docs.djangoproject.com/en/1.7/howto/custom-model-fields/
    # part "Field deconstruction"

    def formfield(self,**kwargs):
        kwargs.update({"widget": Textarea(attrs={'cols':85,'rows':2})})
        self.validators.append(validators.MaxLengthValidator(self.max_length))
        return super(ccLongCharField, self).formfield(**kwargs)


class ccCharField(models.CharField):
    """
    Charfield with
        - new attribute size, used to resize the admin input area
        - default max_length = 55 and
    """
    
    def __init__(self,*args,**kwargs): 

        # Get new attribute size
        self.size = None
        if kwargs.has_key('size'):
            self.size= kwargs['size']
            del kwargs['size']

        # default max_length is 55 characters
        kwargs['max_length']=kwargs.get('max_length',55)
        super(ccCharField,self).__init__(*args,**kwargs) 

        # size value is set to max_lenght is none
        if self.size is None:
            self.size=self.max_length

    def formfield(self,**kwargs):
        """
        define widget size according to the attribute size
        """
        kwargs.update({"widget": TextInput(attrs={'size':self.size})})
        #self.validators.append(validators.MaxLengthValidator(self.max_length))
        return super(ccCharField, self).formfield(**kwargs)


class ccURLField(models.URLField):
    """
    URLField with input size = 86 and default verbose same : web
    """
    def __init__(self,*args,**kwargs):
        if not args:
            kwargs['verbose_name']=kwargs.get('verbose_name','Site web')
        super(ccURLField,self).__init__(*args,**kwargs)

    def formfield(self,**kwargs):
        kwargs.update({"widget": TextInput(attrs={'size':88})})
        #self.validators.append(validators.MaxLengthValidator(self.max_length))
        return super(ccURLField, self).formfield(**kwargs)

class ccEmailField(models.EmailField):
    """
    EmailField with input size = 50 and default verbose same : Mèl
    """

    def __init__(self,*args,**kwargs):
        if not args:
            kwargs['verbose_name']=kwargs.get('verbose_name','Mèl')
        super(ccEmailField,self).__init__(*args,**kwargs)

    def formfield(self,**kwargs):
        kwargs.update({"widget": TextInput(attrs={'size':88})})
        #self.validators.append(validators.MaxLengthValidator(self.max_length))
        return super(ccEmailField, self).formfield(**kwargs)

#-----------------------------------------------------------------------------
#  Date and time Fields
class ccTimeField(models.TimeField):
    """
    Modify the models.TimeField format
    """
    def formfield(self,**kwargs):
        kwargs.update({"widget":TimeInput(format='%H:%M')})
        kwargs.update({"input_formats":('%Hh%M','%H:%M','%Hh')})
        #self.validators.append(validators.MaxLengthValidator(self.max_length))
        return super(ccTimeField, self).formfield(**kwargs)


class ccAdminSplitDateTime(AdminSplitDateTime):
    """
    Used by ccDateTimeField
    """
    def __init__(self,*args,**kwargs):
        super(ccAdminSplitDateTime,self).__init__(*args,**kwargs)
        #self.time_format='%H:%M'
        #self.input_time_formats = ('%H:%M','%Hh%M')
        for widget in self.widgets:
            if widget.__class__.__name__=='AdminTimeWidget':
                widget.format='%H:%M'
            

class ccDateTimeField(models.DateTimeField):
    """
    Modify the models.DateTimeField format
    """
    def __init__(self, *args, **kwargs):
            super(ccDateTimeField, self).__init__(*args, **kwargs)
            self.input_time_formats = ('%H:%M','%Hh%M')
            # If import DateTimeField from db package and not form, it does not
            # include the attributes input_time_formats and input_date_formats

    def formfield(self,**kwargs):
        kwargs.update({"widget":ccAdminSplitDateTime()})
        return super(ccDateTimeField, self).formfield(**kwargs)

#-----------------------------------------------------------------------------
#  Foreign key
class ccForeignKey(models.ForeignKey):
    """
    ForeignKey : default deleting usage is set to PROTECT
    NOT WORKING WITH DJANGO 1.7 AND POSTGRES
    --> currently not to be used
    """
    def __init__(self,*args,**kwargs):
        kwargs['on_delete']=kwargs.get('on_delete',models.PROTECT)
        super(ccForeignKey,self).__init__(*args,**kwargs)

#-----------------------------------------------------------------------------
# test : modify many2many widget dimensions
# filter_horizontal=(nom_du_champ)
from django.contrib.admin.widgets import FilteredSelectMultiple
class ccManyToManyField(models.ManyToManyField):

    #def __init__(self,*args,**kwargs):
    #    super(ccManyToManyField,self).__init__(*args,**kwargs)

    def formfield(self,**kwargs):
        # resize only the first of the 2 boxes
        # The second one is resize via a css file
        # Note : it does not look the same since 1.8 --> 1.10
        kwargs.update({"widget": FilteredSelectMultiple(self.verbose_name,False,attrs={'style':"height: 10em"})})
        return super(ccManyToManyField, self).formfield(**kwargs)


