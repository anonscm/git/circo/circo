# -*- coding: utf-8 -*-
"""
Adding context data specific to circo.
These data can be read from any circo templates.

in order to plug these adding context data, add the following to the settings:
TEMPLATE_CONTEXT_PROCESSORS = global_settings.TEMPLATE_CONTEXT_PROCESSORS + (
    "circo.context_processors.circo_settings",
"""

from django.conf import settings

def circo_settings(request):
    """ Returns context data that comes from the settings.py file
    """
    
    # Get the enabled circo sites and apps from the application list
    circo_sites=[s.split('.')[-1] for s in settings.INSTALLED_APPS if (s.count('circo_sites')>0)]
    circo_apps=[a.split('.')[-1] for a in settings.INSTALLED_APPS if (a.count('circo_apps')>0)]

    # Get the admin mails
    uAdminMails=u''
    for admin in settings.ADMINS:
        uAdminMails=uAdminMails+admin[1]+', '

    return { 'CC_ENV': settings.CC_ENVIRONMENT.upper(),
             'CC_OWNER': settings.CC_SITE_OWNER,
             #'CC_TRAINING_WEB': settings.CC_TRAINING_WEB,
             'CC_SITES': circo_sites,
             'CC_APPS': circo_apps,
             'CC_ADMINS_MAILS':uAdminMails
            }

