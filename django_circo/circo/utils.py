# -*- coding: utf-8 -*-
# circo utils functions

from geopy import Nominatim
from geopy.exc import GeocoderTimedOut

def get_location(strAddress):
    """
    Seach the geolocation of an address, using Nominatim (osm geocoder)
    :param strAddress:
    :return: geolocation
    """
    osmLocator=Nominatim()
    location=None

    try:
        location = osmLocator.geocode(strAddress)
    except GeocoderTimedOut as e:
        print("Error: osm geocode with message "+e.message)

    return location
