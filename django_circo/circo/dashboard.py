# -*- coding: utf-8 -*-
"""
This file was generated with the customdashboard management command, it
contains the two classes for the main dashboard and app index dashboard.

To activate your index dashboard add the following to your settings.py::
    ADMIN_TOOLS_INDEX_DASHBOARD = 'django_circo.dashboard.CustomIndexDashboard'

And to activate the app index dashboard::
    ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'django_circo.dashboard.CustomAppIndexDashboard'
"""

from django.utils.translation import ugettext_lazy as _
from admin_tools.dashboard import modules, Dashboard, AppIndexDashboard
from django.apps import apps
from django.conf import settings

class ccIndexDashboard(Dashboard):
    """
    Custom index dashboard for django_circo.
    """

    APPS_ORDER=['training','expert','project','soft','common']


    def init_with_context(self, context):
        """
        Init the dashboard with the circo_app applications, and with the
        django.contrib application
        """

        lstAppsOrder=self.APPS_ORDER
        dicModules={}

        # parse all the installed application
        # app : app name with package (ex : circo_app.training)
        for app in settings.INSTALLED_APPS:
            # keep only the circo_apps and django.contrib applications
            if ( 'circo_apps.' in app or 'django.contrib.' in app):

                # get app label (module name) ex : 'training'
                strAppLabel = app.split('.')[-1]

                # get appConfig
                appConfig = None
                try :
                    appConfig = apps.get_app_config(strAppLabel)
                except:
                    pass

                # if appConfig, create the ModelList object (= a module in the
                # dashboard), and keep it in a dictionnaty
                if appConfig:

                    module=modules.ModelList(
                        title=appConfig.verbose_name,
                        models=[app+'.*',],
                        title_url='/admin/'+strAppLabel,)
                    # update the application order list if needed:
                    if strAppLabel not in lstAppsOrder:
                        lstAppsOrder.append(strAppLabel)
                    dicModules[strAppLabel]=module
        # end for app in apps.get_apps()

        # Add the modules to the dashbaord, according the the applications
        # order preferences
        for strAppLabelOrdered in lstAppsOrder:
            if dicModules.has_key(strAppLabelOrdered):
                self.children.append(dicModules[strAppLabelOrdered])

        # append a recent actions module
        self.children.append(modules.RecentActions(_('Recent Actions'), 5))


class ccAppIndexDashboard(AppIndexDashboard):
    """
    Custom app index dashboard for django_circo.
    """

    # we disable title because it is redundant with the model list module
    title = ''

    def init_with_context(self, context):
        """
        Dashboard limitation : when a verbose name is defined for an app
        (using the apps.py file) then the __init__ method for the AppIndexDashboard
        does not properly initialize the model list (self.models) : it is
        empty.
        Using the init_with_context method, we can access the app name and
        then rebuild the model list.
        """

        appLabel= context['app_label']
        appConfig = apps.get_app_config(appLabel)
        models=[m.__module__+'.'+m.__name__ for m in appConfig.get_models()]
        self.models=models

        self.children += [
            modules.ModelList(self.app_title, self.models),
            modules.RecentActions(
                _('Recent Actions'),
                include_list=self.get_app_content_types(),
                limit=5
            )
        ]
