# -*- coding: utf-8 -*-

"""
Circo specific forms
"""
from django.forms.models import BaseInlineFormSet

class ccRequiredInlineFormSet(BaseInlineFormSet):
    """
    Generates an inline formset that is required :
    override the form.empty_permitted value of inline form
    """
    def _construct_form(self, i, **kwargs):
        """
        Override the method to change the form attribute empty_permitted
        """
        form = super(ccRequiredInlineFormSet, self)._construct_form(i, **kwargs)
        form.empty_permitted = False
        return form