# -*- coding: utf-8 -*-
"""
Django settings for circo project.

For more information on this file, see
https://docs.djangoproject.com/en/dev/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/dev/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

print BASE_DIR

# Application definition
''''
INSTALLED_APPS = INSTALLED_APPS + (
                  # circo  aapplication : defined in local_settings.py

                  # Tools
                  'admin_tools',                # dashboard
                  'admin_tools.dashboard',      # dashboard
                  'autocomplete_light',         # autocomplete

                  # Django Applications
                  'django.contrib.admin',
                  'django.contrib.auth',
                  'django.contrib.contenttypes',
                  'django.contrib.sessions',
                  'django.contrib.messages',
                  'django.contrib.staticfiles',
                  )
print INSTALLED_APPS
'''

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR,'local','templates'),
            # Don't forget to use absolute paths, not relative paths.
        ],
        #'APP_DIRS': True,
        'OPTIONS': {
            #'debug': DEBUG,
            'context_processors': [
                # Insert your TEMPLATE_CONTEXT_PROCESSORS here or use this
                # list if you haven't customized them:
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                #'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                #'django.template.context_processors.tz',

                'django.template.context_processors.request',
                #'django.core.context_processors.request',
                'circo.context_processors.circo_settings',
                'django.contrib.messages.context_processors.messages',
            ],
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
                'admin_tools.template_loaders.Loader',
            ],

        },
    },
]


#TEMPLATE_DIRS = (
#    os.path.join(BASE_DIR,'local','templates'),
    # Don't forget to use absolute paths, not relative paths.
#    )

# Additional locations for static files
# used by the dev server and by the collectstatic tool
STATICFILES_DIRS=(
    os.path.join(BASE_DIR,'local','static'),
    # Don't forget to use absolute paths, not relative paths.
    )

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/dev/howto/deployment/checklist/

MIDDLEWARE_CLASSES = (                     
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
#    'django.contrib.auth.middleware.RemoteUserMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'circo.middleware.WhodidMiddleware',
)


# Internationalization
# https://docs.djangoproject.com/en/dev/topics/i18n/

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = False

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/dev/howto/static-files/
STATIC_URL = '/static/'

#TEMPLATE_CONTEXT_PROCESSORS = global_settings.TEMPLATE_CONTEXT_PROCESSORS + (
#    "circo.context_processors.circo_settings",
#    "django.core.context_processors.request", #needed for grapelli
#)

SESSION_EXPIRE_AT_BROWSER_CLOSE=True

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
                       'django.contrib.staticfiles.finders.FileSystemFinder',
                       'django.contrib.staticfiles.finders.AppDirectoriesFinder',
                       #    'django.contrib.staticfiles.finders.DefaultStorageFinder',
                       )
#AUTHENTICATION_BACKENDS = (
#    'django.contrib.auth.backends.RemoteUserBackend',
#)


#=============================================================================
# Tools

# admin_tools dashboard
ADMIN_TOOLS_INDEX_DASHBOARD = 'circo.dashboard.ccIndexDashboard'
ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'circo.dashboard.ccAppIndexDashboard'



