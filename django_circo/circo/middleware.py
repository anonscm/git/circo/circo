# -*- coding: utf-8 -*-
"""
Middleware -  run in background.
Automatically fill fields like ..._created_by_id and ..._modified_by_id
Almost entirely taken from https://gist.github.com/mindlace/3918300

to activate it, add the following to the settings :

MIDDLEWARE_CLASSES = (
    ...,
    'circo.middleware.WhodidMiddleware',
    )

"""
from django.db.models import signals
from django.utils.functional import curry

class WhodidMiddleware(object):
    """
    Who did middleware : automatically fill fields like ..._created_by_id
    and ..._modified_by_id
    """
    def process_request(self, request):
        if not request.method in ('GET', 'HEAD', 'OPTIONS', 'TRACE'):
            if hasattr(request, 'user') and request.user.is_authenticated():
                user = request.user
            else:
                user = None

            mark_whodid = curry(self.mark_whodid, user)
            signals.pre_save.connect(mark_whodid,  dispatch_uid = (self.__class__, request,), weak = False)

    def process_response(self, request, response):
        signals.pre_save.disconnect(dispatch_uid =  (self.__class__, request,))
        return response

    def mark_whodid(self, user, sender, instance, **kwargs):
        lstAttrCreated=[x for x in dir(instance) if x.endswith('_created_by_id')]
        lstAttrModified=[x for x in dir(instance) if x.endswith('_modified_by_id')]
        #lstAttrCreatedEnt=[x for x in dir(instance) if x.endswith('_created_by_ent_id')]

        # Other possibility
        # lstAttrCreated=[x for x in dir(instance) if x[3:]='_created_by_id''

        if (len(lstAttrCreated)==1):
            strAttrCreated=lstAttrCreated[0]
            if not getattr(instance, strAttrCreated, None):
                setattr(instance,strAttrCreated[:-3],user) #remove the trailing '_id' from the attr name
                #instance.created_by = user
        if (len(lstAttrModified)==1):
            strAttrModified=lstAttrModified[0]
            if hasattr(instance,strAttrModified):
                setattr(instance,strAttrModified[:-3],user) #remove the trailing '_id' from the attr name
                #instance.modified_by = user
        '''
        if (len(lstAttrCreatedEnt)==1):
            strAttrCreatedEnt=lstAttrCreatedEnt[0]
            if not getattr(instance, strAttrCreatedEnt, None):
                if hasattr(user,'userprofile'):
                    setattr(instance,strAttrCreatedEnt[:-3],user.userprofile.ent) #remove the trailing '_id' from the attr name
        '''