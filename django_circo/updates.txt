first release sept. 2015
content :
    - base to use circo apps and sites. Their use is optionnal, safe the common
        app.
    - common and training apps
        (other apps that will possibly come are experts, projects and softs)
    - 3 sites : Admin, API and focal (training catalog).

release sept 2016
    focal is removed from the circo projet
    A new site is created: consult_training inpired from focal, with some
    modifications :
        Remove use of bootstrap
        Use leaflet.js instead of googlemaps api
    upgrade to django 1.10
    data model :
        remove user/entity link
        add some fields to the training models
    add rss feed to the api site