# -*- coding: utf-8 -*-

from circo_apps.training.models import Training
from django.views import generic
from django.utils import timezone

class FuturTrainingList(generic.ListView):
    model = Training
    template_name = 'consult_training/consult_training_home.html'
    context_object_name = 'futur_training_list'

    def get_queryset(self):
        """
        Returns the trainings that are not finished yet
        """
        return Training.objects.filter(
            tra_dtend__gte=timezone.now()
        )

class TrainingDetail(generic.DetailView):
    model = Training
    template_name = 'consult_training/consult_training_detail.html'

    def get_queryset(self):
        return Training.objects.all()
