
/*https://www.toptal.com/javascript/10-most-common-javascript-mistakes*/
'use strict';

// Global variable : Tile provider
// Here, it is openstreetmap.France
var tileUrl='http://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png';
var tileAttrib='Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';


// when page is loaded, add the map
$(document).ready(function() {

        //create the leaflet frame
    var trainingMap = new L.map('training_detail_map', {
        scrollWheelZoom: false
    });

         // create the tile layer with correct attribution
    var tile = new L.TileLayer(tileUrl, {
        maxZoom: 18,
        attribution: tileAttrib
    });
    tile.addTo(trainingMap)

    var zoom=15;
    var latitude=$("#latitude").text();
    var longitude=$("#longitude").text();

        //var latitude=43.5774;
    //var longitude=1.376;

    trainingMap.setView([latitude, longitude], zoom);
    //trainingMap.addLayer(tile);
    var marker = L.marker([latitude, longitude]).addTo(trainingMap);

} ); // end $(document).ready(function()