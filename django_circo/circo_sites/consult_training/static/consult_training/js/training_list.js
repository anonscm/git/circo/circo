// TODO : find a way to hide empty detail rows.
// TODO : filters and accents
/*https://www.toptal.com/javascript/10-most-common-javascript-mistakes*/
'use strict';

// Global variable : Tile provider
// Here, it is openstreetmap.France
var tileUrl='http://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png'
var tileAttrib='Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';

// Add date filter function.
//  See https://datatables.net/plug-ins/filtering/row-based/range_dates
$.fn.dataTable.ext.afnFiltering.push(
    function( oSettings, aData, iDataIndex ) {

        // works with date format=yyyy-mm-dd
        var sDateLow=Date.parse($('#filter_dtLow').val());
        var sDateHigh=Date.parse($('#filter_dtHigh').val());
        var iDateCol = 3;
        var sStartDate=Date.parse(aData[iDateCol]);

        if ( isNaN(sDateLow) && isNaN(sDateHigh) )
        {
            return true;
        }
        else if ( sDateLow <= sStartDate && isNaN(sDateHigh))
        {
            return true;
        }
        else if ( sDateHigh >= sStartDate && isNaN(sDateLow))
        {
            return true;
        }
        else if (sDateLow <= sStartDate && sDateHigh >= sStartDate)
        {
            return true;
        }
        return false;
    }
);

// when page is loaded, format the data table and add the map
$(document).ready(function() {

    /********************************************************************
    Datatable tuning ************/

    var oTable = new $('#trainings-tbl').DataTable( {

          "search": {
            "regex": true
          },


        responsive: {
            details: {
                type: 'column',
                target: 'tr'
            }
        },

        "bAutoWidth": false,

        // columns options
        "columnDefs": [

            // Responsibe hidding columns priority
            { responsivePriority: 0, targets: 2 }, //date
            { responsivePriority: 1, targets: 6 }, //agglo
            { responsivePriority: 2, targets: -1 }, //links

            { // add the + button to show details at the 1rst column
                className: 'control',
                orderable: false,
                targets:   0,
            },

            { "width": "50%", "targets": 1 },

            { // hide the dtstart date
                "targets": [ 3 ],
                "visible": false,
            },
            // Sort the date column according to the start date one
            {"iDataSort": 3, "aTargets": [ 2 ]},
        ],

        /* Ddefault sorting = start date*/
        "order": [[ 2, "asc" ]],

        "paging": false,

        // add French traduction to the datatable
        "language": {
                "sProcessing":     "Traitement en cours...",
                "sSearch":         "<span title='Filtre sur les colonnes de la liste des formations'>Filtrer:</pan>",
                "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
                "sInfo":           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                "sInfoEmpty":      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                "sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                "sInfoPostFix":    "",
                "sLoadingRecords": "Chargement en cours...",
                "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                "sEmptyTable":     "Aucune donn&eacute;e disponible dans le tableau",
                "oPaginate": {
                    "sFirst":      "Premier",
                    "sPrevious":   "Pr&eacute;c&eacute;dent",
                    "sNext":       "Suivant",
                    "sLast":       "Dernier"
                },
                "oAria": {
                    "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
                    "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
                }
            },

         // Dom organisation
         //"sDom": 'lfrtip'
    } );


    // get the content of ... div and add it to ..; div
    // id="trainings-tbl_filter"
    // id = "date_filter"
    $("#date_filter").children().appendTo("#trainings-tbl_filter");

    /********************************************************************
    EVENTS ************/

    // Event listener to the two range filtering inputs to redraw on input

    $('#filter_dtLow').change( function() {
//    $('#filter_dtstart').blur( function() {
        //valid format : yyyy-mm-dd
           oTable.draw();
    } );

    $('#filter_dtHigh').change( function() {
        //valid format : yyyy-mm-dd
        oTable.draw();
    } );

    $('#trainings-tbl_filter input').keydown(function(event){
        var keyCode = (event.keyCode ? event.keyCode : event.which);
        if (keyCode == 13) {
            this.blur(); // remove focus when enter is pressed
        }
    });

    // Re-init button : Remove search terms and re-init the table
    $('#reset_filters').click(function(){
        $('#filter_dtHigh').val('aaaa-mm-jj');
        $('#filter_dtLow').val('aaaa-mm-jj');
        oTable
         .search( '' )
         .columns().search( '' )
         .draw();
    });

    /********************************************************************
    MAP ***********
    https://switch2osm.org/fr/utilisation-des-tuiles/debuter-avec-leaflet/
    with MapQuest tile proveder:
    http://codepen.io/Jenn/pen/zJnBA
    var marker = L.marker([42.63685, 2.59415]).addTo(map);
    http://patmsweet.com/post/33647369952/linking-leaflet-maps-to-datatables
    */

    //create the leaflet frame
    var trainingMap = new L.map('training_map', {
        scrollWheelZoom: false
    });

     // create the tile layer with correct attribution
    var tile = new L.TileLayer(tileUrl, {
        maxZoom: 18,
        attribution: tileAttrib
    });
    tile.addTo(trainingMap)
    trainingMap.setView([46.5, 3], 5);
    //trainingMap.addLayer(tile);

    //initmap();

    // On parcourt la table pour ajouter les curseurs
    var rows = $("#trainings-tbl").dataTable().fnGetNodes();
    var iId=-10;
    var iLatitude=11;
    var iLongitude=12;
    //maxClusterRadius
    var markers = L.markerClusterGroup({
        showCoverageOnHover:false,
        maxClusterRadius:40});

    oTable.rows().every(function() {
    //      L.marker(row[11], row[12]).addTo(trainingMap);

        var data=this.data();
        //var marker = L.marker([data[iLatitude], data[iLongitude]]).addTo(trainingMap);
        var marker = L.marker([data[iLatitude], data[iLongitude]]);
                //{title: data[1],     // Add a title
                //}
        marker.tra_id=data[10]; // tester si on peut ajouter des attributs

        marker.bindPopup(data[1]); // title


        marker.on('mouseover', function (e) {
            this.openPopup();
        });

        marker.on('click', function (e) {
            oTable.columns(10).search(this.tra_id,true,true).draw();
        });

        /*
        marker.on('mouseout', function (e) {
            this.closePopup();
        });
        */

        markers.addLayer(marker);

    });

    trainingMap.addLayer(markers);

    //events
    markers.on('clusterclick', function (a) {
        // a.layer is actually a cluster
        var markers=a.layer.getAllChildMarkers();
        var sSearch=markers[0].tra_id;
        for ( var i=1; i<markers.length; i++) {
            var marker=markers[i];
            sSearch+='|'+marker.tra_id;
            console.log(marker.tra_id);
        }
        oTable.columns(10).search(sSearch,true,true).draw();
    });


} ); // end $(document).ready(function()

