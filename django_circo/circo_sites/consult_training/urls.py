# -*- coding: utf-8 -*-

from django.conf.urls import include, url
from django.views.generic import TemplateView, RedirectView
from django.conf import settings
from circo_sites.consult_training.views import FuturTrainingList, TrainingDetail

# Redirect to the training consultation home page. Other app consultation pages
# do not exists yet
urlpatterns = [
    url(r'^$', FuturTrainingList.as_view(),name='consult_training_home'),
    url(r'^training/(?P<pk>\d+)/$', TrainingDetail.as_view(), name='consult_training_detail'),
]

