# -*- coding: utf-8 -*-

from rest_framework.decorators import api_view,permission_classes
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework import permissions
#-----------------------------------------------------------------------------
# main api page
# replace by a class
@api_view(('GET',))
@permission_classes((permissions.AllowAny,))
def api_root(request, format=None):
    return Response({
        #"Ici, table des matieres de l'api",
        'Liste des formations': reverse('training-list', request=request, format=format),
    })

# http://www.django-rest-framework.org/topics/browsable-api
