# -*- coding: utf-8 -*-git
from rest_framework.renderers import JSONRenderer,BaseRenderer

class JSONPRenderer(JSONRenderer):
    """
    Renderer which serializes to json,
    wrapping the json output in a callback function.

    This if the content of the python package django-rest-framework-jsonp
    https://github.com/jpadilla/django-rest-framework-jsonp
    with one modification for version compatibility with rest
    request.QUERY_PARAMS --> request.query_params
    """

    media_type = 'application/javascript'
    format = 'jsonp'
    callback_parameter = 'callback'
    default_callback = 'callback'
    charset = 'utf-8'

    def get_callback(self, renderer_context):
        """
        Determine the name of the callback to wrap around the json output.
        """
        request = renderer_context.get('request', None)
        #params = request and request.QUERY_PARAMS or {}
        params = request and request.query_params or {}
        return params.get(self.callback_parameter, self.default_callback)

    def render(self, data, accepted_media_type=None, renderer_context=None):
        """
        Renders into jsonp, wrapping the json output in a callback function.

        Clients may set the callback function name using a query parameter
        on the URL, for example: ?callback=exampleCallbackName
        """
        renderer_context = renderer_context or {}
        callback = self.get_callback(renderer_context)
        json = super(JSONPRenderer, self).render(data, accepted_media_type,
                                                 renderer_context)
        return callback.encode(self.charset) + b'(' + json + b');'


from datetime import datetime
from icalendar import Calendar, Event, vText, vCalAddress
from django.utils.dateparse import parse_datetime

class ICALRenderer(BaseRenderer):
    """

    """
    media_type="text/calendar"
    #media_type='text/plain' # debug
    format = 'ical'
    charset = 'utf-8'
    dtstamp = datetime.now()

    def render(self, data, media_type=None, renderer_context=None):

        cal = Calendar()
        cal.add('prodid', 'formation-calcul.math.cnrs.fr/calendar')
        cal.add('version', '2.0')
        cal.add('name','calendrier formations calcul')

        # data are collection.OrderedDict type
        if data.has_key('results'):
            for event_data in data['results']:
                event=self._get_event(event_data)
                cal.add_component(event)
        else:
            event=self._get_event(data)
            cal.add_component(event)
        return cal.to_ical()

    def _get_event(self,data):
        event = Event()
        event['uid'] = 'formation-calcul.math.cnrs/training/'+unicode(data['tra_id'])
        event['categories'] = data['tra_type']
        event.add('summary', data['tra_title'])
        event.add('dtstart', parse_datetime(data['tra_dtstart']))
        event.add('dtend', parse_datetime(data['tra_dtend']))
        event.add('created', parse_datetime(data['tra_dtcreated']))
        event.add('last-modified', parse_datetime(data['tra_dtmodified']))
        event.add('dtstamp', self.dtstamp)
        #event.add('comment', 'on va mettre le calendrier')
        if data['tra_address']:
            event['location'] = vText(data['tra_address'])
        else:
            event['location'] = vText(data['tra_urbanArea'])
        if data['tra_web']:
            event['url'] = vText(data['tra_web'])
        if data['tra_contact_email']:
            event['organizer'] = vCalAddress('MAILTO:'+data['tra_contact_email'])

        # contact/organizer
        # categories --> type and entities

        return event


"""
Provides XML rendering support.
Taken from http://jpadilla.github.io/django-rest-framework-xml/ , with a
modification to remove control characters
"""

from django.utils import six
from django.utils.xmlutils import SimplerXMLGenerator
from django.utils.six.moves import StringIO
from django.utils.encoding import smart_text
import re


class XMLRenderer(BaseRenderer):
    """
    Renderer which serializes to XML.
    """

    media_type = 'application/xml'
    format = 'xml'
    charset = 'utf-8'
    item_tag_name = 'list-item'
    root_tag_name = 'root'

    def render(self, data, accepted_media_type=None, renderer_context=None):
        """
        Renders `data` into serialized XML.
        """
        if data is None:
            return ''

        stream = StringIO()

        xml = SimplerXMLGenerator(stream, self.charset)
        xml.startDocument()
        xml.startElement(self.root_tag_name, {})

        self._to_xml(xml, data)

        xml.endElement(self.root_tag_name)
        xml.endDocument()
        return stream.getvalue()

    def _to_xml(self, xml, data):
        if isinstance(data, (list, tuple)):
            for item in data:
                xml.startElement(self.item_tag_name, {})
                self._to_xml(xml, item)
                xml.endElement(self.item_tag_name)

        elif isinstance(data, dict):
            for key, value in six.iteritems(data):
                xml.startElement(key, {})
                self._to_xml(xml, value)
                xml.endElement(key)

        elif data is None:
            # Don't output any value
            pass

        else:

            # udpate 09-2016 : remove the control characters
            data=unicode(data)
            illegal_xml_re = re.compile(
                u'[\x00-\x08\x0b-\x1f\x7f-\x84\x86-\x9f\ud800-\udfff\ufdd0-\ufddf\ufffe-\uffff]')
            data = illegal_xml_re.sub('', data)
            # end update 09-2016

            xml.characters(smart_text(data))



