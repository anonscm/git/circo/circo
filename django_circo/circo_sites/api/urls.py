# -*- coding: utf-8 -*-

from django.conf.urls import url ,include
from django.views.generic import TemplateView
from django.conf import settings

#urlpatterns = [r'^api/$',
urlpatterns = [
    # Api Home page and root
    #url(r'^api/$', TemplateView.as_view(template_name="api/api_home.html"),name='api_home'),
    url(r'^$', TemplateView.as_view(template_name="api/api_home.html"),name='api_home'),
    #url(r'^api/root/$', 'circo_sites.api.views.api_root'),
]

apps = settings.INSTALLED_APPS
circo_apps = [app for app in apps if app.startswith('circo_apps')]


# Add training api urls if app is enabled
if ('circo_apps.training' in settings.INSTALLED_APPS):
    urlpatterns += [url(r'^training/', include('circo_apps.training.api.urls')), ]

# Add Expert api urls if expert app is enabled
# Add Project api urls is project app is enabled

# currently not working
#from rest_framework.urlpatterns import format_suffix_patterns
#url(r'', include(format_suffix_patterns(urlpatterns,allowed=['json','api','xml','jsonp','admin'])))
#urlpatterns = format_suffix_patterns(urlpatterns, allowed=['json','api','xml','jsonp','admin'])
