# -*- coding: utf-8 -*-
from rest_framework import generics
from rest_framework import permissions
from rest_framework.viewsets import ReadOnlyModelViewSet
"""
Defines the circo REST API main behaviour.
    - permission : allow any
    - list of available renderers
"""

class ccListAPIView(generics.ListAPIView):
    permission_classes = (permissions.AllowAny,)
    paginate_by = 100
    #allowed_methods = ('GET','HEAD','OPTIONS')
    #renderer_classes = (JSONRenderer,JSONPRenderer,BrowsableAPIRenderer,XMLRenderer)

class ccRetrieveAPIView(generics.RetrieveAPIView):
    permission_classes = (permissions.AllowAny,)
    #renderer_classes = (JSONRenderer,JSONPRenderer,BrowsableAPIRenderer,XMLRenderer)


class ccReadOnlyModelViewSet(ReadOnlyModelViewSet):
    """
    """
    def list(self, request):
        self.serializer_class = self.get_list_serializer_class()
        return super(ccReadOnlyModelViewSet, self).list(request)

    def retrieve(self, request, pk=None):
        self.serializer_class = self.get_detail_serializer_class()
        return super(ccReadOnlyModelViewSet, self).retrieve(request, pk=None)

    def get_list_serializer_class(self):
        if self.list_serializer_class:
            return self.list_serializer_class
        else:
            return self.serializer_class

    def get_detail_serializer_class(self):
        if self.detail_serializer_class:
            return self.detail_serializer_class
        else:
            return self.serializer_class


#from __future__ import unicode_literals
from rest_framework.renderers import JSONRenderer
class JSONPRenderer(JSONRenderer):
    """
    Renderer which serializes to json,
    wrapping the json output in a callback function.

    This if the content of the python package django-rest-framework-jsonp
    https://github.com/jpadilla/django-rest-framework-jsonp
    with one modification for version compatibility with rest
    request.QUERY_PARAMS --> request.query_params
    """

    media_type = 'application/javascript'
    format = 'jsonp'
    callback_parameter = 'callback'
    default_callback = 'callback'
    charset = 'utf-8'

    def get_callback(self, renderer_context):
        """
        Determine the name of the callback to wrap around the json output.
        """
        request = renderer_context.get('request', None)
        #params = request and request.QUERY_PARAMS or {}
        params = request and request.query_params or {}
        return params.get(self.callback_parameter, self.default_callback)

    def render(self, data, accepted_media_type=None, renderer_context=None):
        """
        Renders into jsonp, wrapping the json output in a callback function.

        Clients may set the callback function name using a query parameter
        on the URL, for example: ?callback=exampleCallbackName
        """
        renderer_context = renderer_context or {}
        callback = self.get_callback(renderer_context)
        json = super(JSONPRenderer, self).render(data, accepted_media_type,
                                                 renderer_context)
        return callback.encode(self.charset) + b'(' + json + b');'