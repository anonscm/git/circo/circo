CIRCO LOCAL CONFIGURATION

---------------------------------------------------------------------------------
SETTINGS

file ..../django_circo/local/local_settings.py

Create the file if it does not exist : copy local_settings_exemple.py and modify it

The following settings must be defined for each installation : 

DATABASES : connection to the database to use
INSTALLED_APPS : add or remove some of the circo_apps or circo_sites. Note that 'circo' and 'circo_apps.common' are mandatory
SECRET_KEY : generate a new one for each production environment,
STATIC_ROOT : where the static files (css, js, img) are collected
DEBUG : set it to false in production environment
CC_SITE_OWNER : name of the organization who owns the site ('branding')  
CC_ENVIRONMENT : prod, dev or test

---------------------------------------------------------------------------------
COLORS, templates, css

update the prod, dev or test header and title colors by copying and modifying the files
django_circo/circo/static/circo/css/prod.css dev.css and dev.css
in the django_circo/local/static/circo/css/ directory

Any circo css file can be overridden in django_circo/local/static/circo/css/ directory
Any html file can be overridden in django_circo/local/templates/circo directory